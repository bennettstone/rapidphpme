<?php
/**
 * __example-config.php
 * Src https://dericcain.com/deploying-laravel-5-with-rocketeer
 *
 * @version 1.0
 * @date 1/14/16 9:03 AM
 * @package findparticipants.com
 */
/*
use Rocketeer\Services\Connections\ConnectionsHandler;

return [

	'application_name' => 'MyApp',

	'plugins'          => [],

	'logs'             => function (ConnectionsHandler $connections) {
		return sprintf('%s-%s-%s.log', $connections->getConnection(), $connections->getStage(), date('Ymd'));
	},

	// The default remote connection(s) to execute tasks on
	'default'          => ['staging'],

	'connections'      => [
		'production' => [
			'host'      => 'myapp.com',
			'username'  => 'user',
			'password'  => '',
			'key'       => '',
			'keyphrase' => '',
			'agent'     => '',
			'db_role'   => true,
		],
		'staging' => [
			'host'      => 'staging.myapp.com',
			'username'  => 'user',
			'password'  => '',
			'key'       => '',
			'keyphrase' => '',
			'agent'     => '',
			'db_role'   => true,
		],
	],

	'use_roles'        => false,

	// Contextual options
	'on'               => [
		'stages'      => [],
		'connections' => [
			'staging' => [
				'remote' => [
					'root_directory' => '/srv/users/serverpilot/apps',
					'app_directory'  => 'myapp',
				],
				'scm' => [
					'branch' => 'dev'
				],
				'hooks' => [
					'before' => [
						'setup'   => [],
						'deploy'  => [],
						'cleanup' => [],
					],
					'after'  => [
						'setup'   => [],
						'deploy'  => [
							'ln -s /srv/users/serverpilot/apps/myapp/.env /srv/users/serverpilot/apps/myapp/current/.env ',
							'chown -R serverpilot:serverpilot /srv/users/serverpilot/apps/myapp',
						],
						'cleanup' => [],
					],
				],
			],
			'production' => [
				'remote' => [
					'root_directory' => '/var/www',
					'app_directory'  => 'myapp.com',
				],
				'scm' => [
					'branch' => 'master'
				],
				'hooks' => [
					'before' => [
						'setup'   => [],
						'deploy'  => [],
						'cleanup' => [],
					],
					'after'  => [
						'setup'   => [],
						'deploy'  => [
							'ln -s /var/www/myapp.com/.env /var/www/myapp.com/current/.env ',
						],
						'cleanup' => [],
					],
				],
			]
		],
	],
];
*/