<?php

return [

    // Tasks
    //
    // Here you can define in the `before` and `after` array, Tasks to execute
    // before or after the core Rocketeer Tasks. You can either put a simple command,
    // a closure which receives a $task object, or the name of a class extending
    // the Rocketeer\Abstracts\AbstractTask class
    //
    // In the `custom` array you can list custom Tasks classes to be added
    // to Rocketeer. Those will then be available in the command line
    // with all the other tasks
    //////////////////////////////////////////////////////////////////////

    // Tasks to execute before the core Rocketeer Tasks
    'before' => [
        'setup'   => [],
        'deploy'  => [],
        'cleanup' => [
            function($task) {

                //Upload any .env files that match the stage for this release
                if( !file_exists( base_path().'/.env.'.$task->connections->getStage().'.php' ) )
                {
                    $task->command->error( base_path().'/.env.'.$task->connections->getStage().'.php does not exist!' );
                }
                else
                {
                    $task->upload( base_path().'/.env.'.$task->connections->getStage().'.php', $task->releasesManager->getCurrentReleasePath().'/.env.'.$task->connections->getStage().'.php');
                }
            }
        ],
    ],

    // Tasks to execute after the core Rocketeer Tasks
    'after'  => [
        'setup'   => [],
        'deploy'  => [],
        'cleanup' => [
            function($task) {

                if( $this->app['config']->get( 'rocketeer::wasp_key' ) || $this->app['config']->get( 'rocketeer::emails' ) )
                {
                    $task->command->info( 'Deployment success, notification pending...' );

                    // Get user name
                    $user = $task->localStorage->get( 'notifier.name' );
                    if( !$user )
                    {
                        $user = $task->command->ask( "Who is deploying?\r\n" );
                        $task->localStorage->set( 'notifier.name', $user );
                    }
                    $connection = $task->connections->getConnection();
                    $server = $task->connections->getServer();
                    $credentials = $task->connections->getServerCredentials( $connection, $server );
                    $stage = $task->connections->getStage();
                    $host = $credentials['host'];
                    if( $stage )
                    {
                        $connection = $stage.'@'.$connection;
                    }

                    $repository = $task->connections->getRepositoryCredentials();

                    $params = [
                        'name' => $this->app['config']->get( 'rocketeer::application_name' ),
                        'environment' => $connection,
                        'stage' => $stage,
                        'branch' => $task->connections->getRepositoryBranch(),
                        'server' => $host,
                        'emails' => $this->app['config']->get( 'rocketeer::emails' ),
                        'user' => $user,
                        'release' => $task->releasesManager->getCurrentRelease(),
                        'repository' => $repository['repository'],
                    ];

                    $wasp_key = $this->app['config']->get( 'rocketeer::wasp_key' );
                    if( $wasp_key )
                    {
                        $ch = curl_init();
                        curl_setopt( $ch, CURLOPT_URL, 'https://wasp.io/requests/webhooks/deployment/' . $wasp_key );
                        curl_setopt( $ch, CURLOPT_POST, true );
                        curl_setopt( $ch, CURLOPT_HEADER, false );
                        curl_setopt( $ch, CURLOPT_TIMEOUT, 2 );
                        curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( array( 'parameters' => $params ) ) );
                        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
                        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
                        $response = curl_exec( $ch );
                        if( curl_getinfo( $ch, CURLINFO_HTTP_CODE ) > 200 )
                        {
                            $task->command->error( 'Unable to send notification to Wasp.  Please check your configuration details and API key.' );
                        }
                        curl_close( $ch );
                    }
                    //end notify wasp

                    //var_dump($params);
                    if( $this->app['config']->get( 'rocketeer::emails' ) )
                    {
                        $message = '<p>New deployment to '. $params['stage'] .' from branch '. $params['branch'] .'</p>';
                        $message .= '<pre>'. print_r( $params, true ) . '</pre>';

                        $mail = new PHPMailer();
                        $mail->isHTML( true );

                        $smpt = $this->app['config']->get( 'rocketeer::smtp' );
                        if( !empty( $smpt ) )
                        {
                            $mail->isSMTP();
                            $mail->SMTPDebug = true;
                            $mail->Host = $smpt['host'];
                            $mail->SMTPAuth = true;
                            $mail->Username = $smpt['user'];
                            $mail->Password = $smpt['password'];
                            $mail->SMTPSecure = 'tls';
                            $mail->Port = $smpt['port'];
                        }

                        $mail->setFrom( 'notifications@mail.' . $this->app['config']->get( 'rocketeer::application_name' ) );
                        foreach( $params['emails'] as $email )
                        {
                            $task->command->info( 'Added '. $email.' to deployment notification!' );
                            $mail->addAddress( $email );
                        }

                        $mail->Subject = 'Deployment success for '. $params['name'];
                        $mail->Body = $message;
                        //$mail->isHTML( true );

                        if( !$mail->send() )
                        {
                            $task->command->error( 'Deployment notification failed :(' );
                        }
                        else
                        {
                            $task->command->info( 'Deployment notification issued!' );
                        }
                    }
                    //end send email

                } //end status to be sent SOMEWHERE

            },
        ],
    ],

    // Custom Tasks to register with Rocketeer
    'custom' => [],

];