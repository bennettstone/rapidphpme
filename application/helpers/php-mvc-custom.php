<?php
/**
 * php-mvc-custom.php
 * Demo functions and usage examples
 *
 * @version 1.0
 * @date 08-May-2013
 * @package RapidPHPMe
 **/

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );

function extra_gaq()
{
?>
<script type="text/javascript">
//Google analytics event tracking
function track_action(category, event, string)
{
    if(typeof _gaq == 'undefined'){
        console.log('_gaq is undefined');
    } else {
        _gaq && _gaq.push(['_trackEvent', category, event, string]);
    }
}
</script>
<?php
}

//Function used in /admin/manage areas for textarea tabs
function js_constant()
{
?>
<script type="text/javascript">
var site_options = {
    base: '<?php echo BASE; ?>', 
    debug: <?php echo ENVIRONMENT == 'production' ? 'false' : 'true'; ?>
};
</script>
<?php
}
add_output( 'header', array( 'js_constant' ) );