<?php
/**
 * 404.php
 * Default fallback page when requested page is not found
 *
 * @version 2.0
 * @date 15-Feb-2013
 * @package RapidPHPMe
 **/
if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );


/**
 * Handle htaccess redirects for 400-600 server errors
 */
$status = isset( $_SERVER['REDIRECT_STATUS'] ) ? $_SERVER['REDIRECT_STATUS'] : 400;
$error_codes = array(
    400 => array( '400 Bad Request', 'The request cannot be fulfilled due to bad syntax.' ),
    403 => array( '403 Forbidden', 'The server has refused to fulfil your request.' ),
    404 => array( '404 Not Found', 'The page you requested was not found on this server.' ),
    405 => array( '405 Method Not Allowed', 'The method specified in the request is not allowed for the specified resource.' ),
    408 => array( '408 Request Timeout', 'Your browser failed to send a request in the time allowed by the server.' ),
    500 => array( '500 Internal Server Error', 'The request was unsuccessful due to an unexpected condition encountered by the server.' ),
    502 => array( '502 Bad Gateway', 'The server received an invalid response while trying to carry out the request.' ),
    504 => array( '504 Gateway Timeout', 'The upstream server failed to send a request in the time allowed by the server.' ),
);

if( isset( $error_codes[$status] ) )
{
    $title = $error_codes[$status][1];
    //Drop the status code!
    set_status_header( $status );
}
else
{
    $title = 'Error 404: Page Not Found';
    //Drop the status code!
    set_status_header( 404 );
}

/**
 * Since this page is the redirect for both 404 AND fatal script errors,
 * but displaying PHP errors is a security risk, run a redirect to clear the display error
 * back to this page
 */
if( !empty( $_GET['type'] ) && !empty( $_GET['e'] ) )
{
    //And redirect
    header( 'Location: '. BASE .'/404/error/' );
    exit;
}

view( 'header', array( 'title' => $title ) );
?>

<div class="row">
    <div class="small-12 columns">
        
        <div class="row">
            <div class="small-12 columns">
                <h1>Epic Fail</h1>
                <h2>So here's the deal...</h2>
            </div>
        </div>
    
        <div class="row">
            <div class="medium-12 columns">
                <?php
                if( empty( $_GET['type'] ) && !isset( $error_codes[$status] ) )
                {
                    echo '<p>We sent your request down a hill in a lawnchair on a skateboard.</p><p> It didn\'t come back, but we hope you do. We are fixing the problem.</p>';
                }
                elseif( isset( $error_codes[$status] ) )
                {
                    echo '<p>'. $error_codes[$status][1] .'</p>';
                }
                else
                {
                    echo '<p>Sorry about that. Our engineers are working on a time machine to prevent future problems.</p>';
                }
                ?>
                <p><a href="<?php echo BASE; ?>" alt="Home"><strong>For now, it might be best to just go home.</strong></a></p>
            </div>
        </div>
    </div>
    
</div>

<?php view( 'footer' ); ?>