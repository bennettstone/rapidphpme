<?php
/**
 * environments.php
 * Global environmental variables used to determine
 * the appropriate configuration files to load
 *
 * @version 1.0
 * @date 30-Nov-2014
 * @package RapidPHPMe
 **/    

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );

switch( $_SERVER['HTTP_HOST'] )
{
    case 'localhost':
    case 'localhost:8888':
        define( 'ENVIRONMENT', 'development' );
    break;
    
    case 'dev.yoursite.com':
        define( 'ENVIRONMENT', 'staging' );
    break;
    
    default:
        define( 'ENVIRONMENT', 'production' );
    break;
}

/* End of file environments.php */
/* Location: /application/config/environments.php */