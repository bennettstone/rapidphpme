<?php
/**
 * files.php
 *
 * @version 2.0
 * @date 01-Apr-2015
 * @package RapidPHPMe
 **/

return [

	//local, s3
	'storage' => 'local',

	'discs' => [

		'local' => [

			//Path to upload
			'path' => ASSETS_ROOT,
		],

		's3' => [

			'key' => '',

			'secret' => '',

			'region' => '',

			'bucket' => '',

		],

	],

];