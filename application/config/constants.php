<?php
/**
 * constants.php
 * Allows user defined constants to be added via config
 * Takes key => value pairs
 * 'NAME' => 'bennett' into...
 * define( 'NAME', 'bennett' );
 *
 * @version 2.0
 * @date 04-Dec-2014
 * @package RapidPHPMe
 **/

return [];

/* End of file constants.php */
/* Location: /application/config/constants.php */