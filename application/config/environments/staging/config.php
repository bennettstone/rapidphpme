<?php
/**
 * config.php
 * Global configuration variables
 *
 * @version 2.0
 * @date 30-Nov-2014
 * @package RapidPHPMe
 **/

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );

return [
    
    
    /*******************************
    App URI
    ********************************/
    
    'uri' => 'http://'. $_SERVER['HTTP_HOST'], 
    
    
    /*******************************
    Debug mode
    ********************************/
    
    'debug' => true, 
    
    
    /*******************************
    Site in subdirectory?
    ********************************/
    
    'subdir' => false, 
    
    
    /*******************************
    IF the site is in a subdirectory, you'll need to say
    "which one"
    ********************************/
    
    'dirname' => '', 
    
];

/* End of file config.php */
/* Location: /application/config/config.php */