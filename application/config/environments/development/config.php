<?php
/**
 * config.php
 * Global configuration variables
 *
 * @version 2.0
 * @date 30-Nov-2014
 * @package RapidPHPMe
 **/

return [
    
    'debug' => true, 
    
    'log_errors' => true, 
            
    'subdir' => true, 
    
    'dirname' => '/'.basename( ROOT ), 
    
    'show_timers' => ( isset( $_GET['debug'] ) ? true : false ),  
    
    'wasp_full' => false,

];

/* End of file config.php */
/* Location: /application/config/config.php */