<?php
/**
 * config.php
 * Global configuration variables
 *
 * @version 2.0
 * @date 30-Nov-2014
 * @package RapidPHPMe
 **/

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );

return [

	'wasp_full' => true,

];

/* End of file config.php */
/* Location: /application/config/config.php */