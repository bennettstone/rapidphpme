<?php
/**
 * autoload.php
 * Files to automatically include as accessible site-wide
 *
 * @version 2.0
 * @date 30-Jun-2014
 * @package RapidPHPMe
 **/    

return [];

/* End of file autoload.php */
/* Location: /application/config/autoload.php */