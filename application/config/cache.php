<?php
/**
 * cache.php
 * Global configuration variables
 *
 * @version 2.0
 * @date 30-Nov-2014
 * @package RapidPHPMe
 **/

/*******************************
Cache settings
Assumimg "cache" is specified in autoload.php, the following parameters matter
Supported: files, sqlite, auto, apc, wincache, xcache, memcache, memcached
Path should have trailing slash, and a directory named "cache" will be created automatically if
the location exists
********************************/

return [

    
    //Default cache driver (files, sqlite, auto, apc, wincache, xcache, memcache, memcached)
    'driver' => 'auto', 
    
    //Default time to expire cache items (1 hr)
    'expiry' => ( 60 * 60 ), 
    
    //Path to store cache files
    'path' => APP .'cache', 
    
    //If using memcached, you'll need to specify the driver details
    //A host, port, and weight is required for any/all options
    'memcached' => [
        
        [
            'host' => '127.0.0.1', 
            'port' => 11211, 
            'weight' => 100 
        ],
        
        [
            'host' => 'new.host.ip', 
            'port' => 11211, 
            'weight' => 100
        ]
        
    ],
    
    //Specify if we should auto-append cachenotes to cached output
    'debug' => false,    
    
];

/* End of file cache.php */
/* Location: /application/config/cache.php */