<?php
/**
 * config.php
 * Global configuration variables
 *
 * @version 2.0
 * @date 30-Nov-2014
 * @package RapidPHPMe
 **/

return [
    
    
    /*******************************
    Name of the website
    ********************************/
    
    'sitename' => 'SITE_NAME', 
    
    
    /*******************************
    App URI
    ********************************/
    
    'uri' => 'http://'. $_SERVER['HTTP_HOST'], 
    
    
    /*******************************
    Debug mode
    ********************************/
    
    'debug' => false, 
    
    
    /*******************************
    Use Wasp webservice, or local reporting
    ********************************/
    
    'wasp_full' => false, 
    
    
    /*******************************
    IF using the Wasp webservice, the project API key
    ********************************/
    
    'wasp_api_key' => 'WASP_API_KEY', 
    
    
    /*******************************
    Define if the errors should be written to a file
    Define if function trace logs should be generated to the /logs/ directory
    ********************************/
    
    'log_errors' => false, 
    
    
    /*******************************
    Whom to email error logs to.  If empty, the errors 
    just don't get emailed
    ********************************/
    
    'error_emails' => array(),
    
    
    /*******************************
    Maximum size of error log files (IF USED) in mb
    ********************************/
    
    'log_size' => 1, 
    
    
    /*******************************
    Encryption keys
    Make sure it's 16, 24, or 32 characters long
    You can use something like the following to generate a key:
    
    echo substr( md5( mt_rand() ),0, 32 );
    
    If you plan on using the same data across multiple locations, it'd be wise
    to set the key here and keep it out of environmental configuration files, otherwise
    your data won't be able to be decrypted
    ********************************/
    
    'encryption_key' => 'ENCRYPTION_KEY', 
    
    
    /*******************************
    Define the default CRSF input name used for form input validation
    ********************************/
    
    'crsf_name' => 'data_token', 
    
    
    
    /*******************************
    Show the load timer
    ********************************/
    
    'show_timers' => false,
    
    
    
    /*******************************
    Timezone, supported timezones may be found at:
    http://php.net/manual/en/timezones.php
    ********************************/
    
    'timezone' => 'America/New_York', 
    
    
    /*******************************
    Site in subdirectory?
    ********************************/
    
    'subdir' => false, 
    
    
    /*******************************
    IF the site is in a subdirectory, you'll need to say
    "which one"
    ********************************/
    
    'dirname' => '', 
    
    
    /*******************************
    Default page title separator
    ********************************/
    
    'title_separator' => ' | ', 
    
    
    /*******************************
    SMTP email info, if left empty the constants just aren't set or used
    
    This is used for PHPMailer
    ********************************/
    
    'smtp' => [
        
        //SMTP user
        'user' => '', 
        
        //SMTP password
        'password' => '', 
        
        //SMTP location
        'host' => '', 
        
        //SMTP port
        'port' => ''
    ],
    
];

/* End of file config.php */
/* Location: /application/config/config.php */