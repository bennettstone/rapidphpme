<?php
/**
 * routes.php
 *
 * Define custom routes to nested URI paths
 * Default contains no routes, requiring all pages be at yourwebsite.com/view-name
 * Specifying additonal routes allows nesting such as:
 * yourwebsite.com/users/login
 *
 * $routes['directory/view'] = array( 'uri', 'uri', ':get-variable' );
 *
 * For example, http://yoursite.com/catalogue/archives/2013/06/25/ can be mapped to the /views/magazines/index.php as:
 * $routes['magazines/index'] = array( 'catalogue', 'archives', ':year', ':month', ':day' );
 * Without custom mapping, the URI above would be accessible at: http://yoursite.com/magazines/index/?year=2013&month=05&day=25
 *
 * You may also specify a controller rather than a view path:
 * Mapping http://yoursite.com/contact/ to the /application/controllers/public/general.php controller
 * which has a function called "controller_use_me":
 * $routes['use_me'] = array( 'contact' );
 *
 * @version 1.0
 * @date 25-Jun-2013
 * @package RapidPHPMe
 **/


Router::set( 'home', array(
	'home',
	':message'
) );

Router::set( '404', array(
	'404',
	':type',
	':details'
) );

/* End of file routes.php */
/* Location: application/core/routes.php */