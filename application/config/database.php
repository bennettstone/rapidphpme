<?php
/**
 * database.php
 * Database connection details
 *
 * @version 2.0
 * @date 30-Nov-2014
 * @package RapidPHPMe
 **/

return [
    
    //Table prefix
    'prefix' => '',
        
    //Database host
    'host' => '', 
    
    //Database username
    'username' => '', 
    
    //Database password
    'password' => '', 
    
    //Database
    'database' => '',

	'tables' => [

		'USERS' => 'users',

	]
    
];

/* End of file database.php */
/* Location: /application/config/database.php */