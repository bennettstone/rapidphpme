<?php
/**
 * autoload.php
 * Files to automatically include as accessible sitewide
 *
 * @version 2.0
 * @date 30-Jun-2014
 * @package RapidPHPMe
 **/    

/**
 * To include additional classes or files, add each desired filename to the array below
 * Required classes are loaded in the includes/autoloader.php loadscript
 *
 * The following options may be autoloaded by name:
 * forms                Form helper
 * meta                 CSS/JS script loading, html meta tag output
 * files                File helper
 * email                PHPMailer
 * html                 Asst html functions: blacklists, word counts, etc...
 * urls (or "url")      Autolink text, display/use current url, etc...
 * include              Include helper or class
 * users                Entire user system
 * cache                Load PHPFastCache
 * images               Load ZebraImage for image manipulation
 * slug                 URL Slug helper to ensure unique CMS created uris
 * sendgrid             Loads configuration for sendgrid
 */
return [
    
    'include', 
    
    'meta', 
    
    USER_HELPERS .'php-mvc-custom.php', 
    
    'forms', 
    
    'email', 
    
    'users', 
    
    'html', 
    
    'url',

];

/* End of file autoload.php */
/* Location: /application/config/autoload.php */