<?php
/**
 * Copyright 2011 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );

$user_facebook_permissions = array();
$user_facebook_path = str_replace( ROOT, BASE, dirname(__FILE__) );
define( 'FB_PATH', $user_facebook_path );
define( 'FB_APP_ID', 'yourappID' );
define( 'FB_APP_SECRET', 'yourappsecret' );
//https://developers.facebook.com/docs/reference/api/user/
define( 'FACEBOOK_PERMS', 'email,user_birthday,user_location,user_website,user_about_me' );
define( 'FB_REDIRECT', USER_DEFAULT_REDIRECT );


/**
 * Display facebook auth button
 *
 * @access public
 * @param string text
 * @param string size
 * @return string fb-login-button
 *
 */
if( !function_exists( 'facebook_button' ) )
{
    function facebook_button( $text = 'Connect With Facebook', $size = 'medium' )
    {
        echo '<div class="facebook_auth">' . PHP_EOL;
        if( !isset( $_SESSION['user_id'] ) || !isset( $_SESSION['fb_'.FB_APP_ID.'_user_id'] ) )
        {
            echo '<div class="fb-login-button" onlogin="javascript:CallAfterLogin();" size="'.$size.'" scope="'.FACEBOOK_PERMS.'">'.$text.'</div>';
        }
        else
        {
            $logout_link = USER_DEFAULT_REDIRECT . '/logout/true';
            echo '<div class="fb-login-button" size="'.$size.'">';
            echo '<a href="'.$logout_link.'">Logout</a>';
            echo '</div>';
        }
        echo '</div>';
        facebook_scripts();
    }
}


/**
 * Output facebook asyc scripts and fb-root div
 *
 * @access public
 * @param none
 * @return string
 *
 */
if( !function_exists('facebook_scripts') )
{
    function facebook_scripts()
    {
        echo '<div id="fb-root"></div>';
        echo '<script type="text/Javascript">
        var authBase = "' . FB_PATH . '";
        window.fbAsyncInit = function() {
        FB.init({appId: \''.FB_APP_ID.'\',cookie: true,xfbml: true,channelUrl: \''.FB_REDIRECT.'\',oauth: true});};
        (function() {var e = document.createElement(\'script\');
        e.async = true;e.src = document.location.protocol +\'//connect.facebook.net/en_US/all.js\';
        document.getElementById(\'fb-root\').appendChild(e);}());
        </script>';
        
        //Add the scripts to process
        echo '<script type="text/javascript" language="Javascript" src="' . FB_PATH . '/facebook.js"></script>';
    
    }
}


/**
 * Login OR register user with facebook
 *
 * @access public
 * @param string
 * @return bool
 *
 */
if( !function_exists('facebook_confirm') )
{
    function facebook_confirm()
    {
            
        require( 'facebook.php' );
        // Create our Application instance (replace this with your appId and secret).
        $facebook = new Facebook(array(
          'appId'  => FB_APP_ID,
          'secret' => FB_APP_SECRET,
        ));

        // Get User ID
        $user = $facebook->getUser();

        // We may or may not have this data based on whether the user is logged in.
        //
        // If we have a $user id here, it means we know the user is logged into
        // Facebook, but we don't know if the access token is valid. An access
        // token is invalid if the user logged out of Facebook.

        if ($user)
        {
            try
            {
                // Proceed knowing you have a logged in user who's authenticated.
                $user_profile = $facebook->api('/me');
            }
            catch (FacebookApiException $e)
            {
                $user = null;
            }
            
            $total = user_exists( 'user_id', array( 'user_email' => $user_profile['email'], 'user_fb_id' => $user_profile['id'] ) );
            if( $total )
            {

                $params['email'] = $user_profile['email'];
                $params['fb_id'] = $user_profile['id'];
                $login = user_system_login( $params );
                
                //Display the FB response if debug is enabled
                if( DEBUG )
                {
                    echo '<pre>';
                    print_r( $login );
                    echo '</pre>';   
                }
            }
            else
            {
                $registration_data = array(
                   'user_email' => array( 
                            'value' => $user_profile['email'], 
                            'required' => true
                    ),
                    'user_name' => array(
                        'value' => $user_profile['name']
                    ),
                   'user_fb_id' => array( 
                        'value' => $user_profile['id'], 
                        'required' => true
                    ), 
                    'user_approved' => array(
                        'value' => 1
                    ), 
                    'is_facebook' => array(
                        'value' => true
                    )
                );

                $register = user_system_registration( $registration_data );
                if( $register === true )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}

/**
 * Process js calls to auth
 *
 * @access public
 * @param string
 * @return function call
 */

if( isset( $_POST['login'] ) && $_POST['login'] == 'true' && isset( $_POST['fb'] ) && $_POST['fb'] == 'true' )
{
    return facebook_confirm();
}