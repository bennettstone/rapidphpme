/* ________________ Handle FB Auth ___________________________ */
function AjaxResponse()
{
    var fbParams = 'login=true&fb=true';
    var delay = 3000;
    jQuery.ajax({
        type: "POST",
        url: "",
        data:fbParams,
        success:function(data){
            setTimeout(function(){
            window.location.reload();
            }, delay);
        },
        error:function (xhr, ajaxOptions, thrownError){
            $(".facebook_auth").html(thrownError); //Error
        }
    });
}

function CallAfterLogin(){
    FB.login(function(response) {		
        if (response.status === "connected") 
        {
            $(".fb-login-button").hide(); //hide login button once user authorize the application
        	$(".facebook_auth").html('Please Wait, Connecting...'); //show loading image while we process user
        	
            FB.api('/me', function(data) {
                if(data.email == null)
                {
                    //Facbeook user email is empty, you can check something like this.
                    alert("You must allow us to access your email id!"); 
                    $(".fb-login-button").show(); //Show login button 
                	$(".facebook_auth").html(''); //reset element html
                }else{
                    AjaxResponse();
                }
            });
        }
    });
}