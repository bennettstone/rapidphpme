<?php
/**
 * user-config.php
 * Assigns variables used for the phpmvc-user-system plugin
 * Assigns default redirects
 * Assigns system language
 *
 * @version 1.0
 * @date 16-Oct-2012
 * @package RapidPHPMe-user-system
 **/

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );

//http path to these files
define( 'USER_PATH', BASE .'/application/plugins/'.basename( dirname( __FILE__ ) ) );
//Default redirect if nothing else is specified
define( 'USER_DEFAULT_REDIRECT', 'http://'.$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
//Location users should be taken to on logins, or level changes
define( 'USER_DEFAULT_MAIN', BASE . '/users/dashboard/' );
//Default outgoing email address used for system messages
define( 'USER_EMAIL_REPLYTO', 'no-reply@'. strtolower( str_replace( ' ', '', SITENAME ) ) .'.com' );
//Specify if activation email must be submitted [true/false]
define( 'USER_REQUIRE_ACTIVATION', true );
//Path to activation script.  Route used: $routes['users'] = array( 'users', 'activate', ':user', ':activ_code' );
define( 'USER_ACTIVATION_BASE', BASE .'/users/activate/' );
//Specify if invalid login attempts should be logged and blocked
define( 'USE_LOCKDOWN', true );
//Maximum number of lockdowns before lockout
define( 'USER_MAX_LOCKDOWNS', 4 );
//Duration after lockdown to prevent login
define( 'USER_LOCKDOWN_TIMEOUT', '+30 minutes' );
//specify cookie timeout in days (default is 10 days)
define( 'COOKIE_TIME_OUT', 10 );

//User levels
define( "ADMIN_LEVEL", 5 );
define( "EDITOR_LEVEL", 3 );
define( "USER_LEVEL", 1 );
define( "INACTIVE_USER", 0 );

//Define the users table name if not already defined
if( !defined( 'USERS' ) )
{
    define( 'USERS', strtolower( SITENAME ) .'_users' );
}
//Define the lockdown table name if not already defined
if( !defined( 'LOCKDOWNS' ) )
{
    define( 'LOCKDOWNS', strtolower( SITENAME ) . '_lockdowns' );
}


//Array of user levels for select menus
global $admin_user_levels;
$admin_user_levels = array(
    0 => 'Inactive', 
    1 => 'User', 
    3 => 'Editor', 
    5 => 'Admin'
);

//Specify if admins should get new email for each user
define( 'ADMIN_NOTIFY', false );
//Comma separated list of people to be notified of site actions if ADMIN_NOTIFY == true
define( 'ADMIN_LIST', 'email-one@email.com, email-two@email.com' );

//Define CRSF input name for registration forms
define( 'REG_CRSF_INPUT_NAME', CRSF_INPUT_NAME .'_reg' );
//Define CRSF input name for login forms
define( 'LOG_CRSF_INPUT_NAME', CRSF_INPUT_NAME .'_log' );
//Define CRSF input name for password reset forms
define( 'RES_CRSF_INPUT_NAME', CRSF_INPUT_NAME .'_res' );

//Include facebook auth? (Additional FB settings in /facebook/facebook-config.php)
define( 'FB_ACTIVE', false );

//LANGUAGE SETTINGS
define( 'USER_LOGIN_INACTIVE', 'Your account has not been activated' );
define( 'USER_INVALID_LOGIN', 'Invalid login.' );
define( 'USER_LOGIN_INVALID_PASS', 'Invalid password' );
define( 'USER_LOCKOUT', 'Your account has been locked down for 30 minutes due to too many invalid access attempts' );
define( 'USER_LOGIN_INVALID_USERNAME', 'Invalid username or email' );
define( 'EMPTY_REGISTRATION', 'No data was submitted.' );
define( 'INVALID_TOKEN', 'An invalid CRSF token was detected.' );
define( 'REQUIRED_FIELD_VALIDATION', 'A required field is missing' );
define( 'USER_ALREADY_EXISTS', 'This email address has already been registered' );
define( 'USER_NO_PASSWORD', 'A valid password is required' );
define( 'USER_NO_EMAIL', 'A valid email is required' );
define( 'USER_PRELIM_ADD', 'User added!' );
define( 'USER_CERTAIN_ADDED', 'User \'really\' added' );
define( 'USER_ACTIVATE_NOT_FOUND', 'No account located.' );
define( 'USER_ACTIVATE_SUCCESS', 'Account successfully activated.' );
define( 'USER_INVALID_ACTIVATION', 'Invalid activation attempt.' );
define( 'USER_RESET_NO_EMAIL', 'You must enter a valid email to reset your password.' );
define( 'USER_PASSWORD_RESET', 'Password reset successfully and sent to your email.' );
define( 'USER_PASS_RESET_BUTTON', 'Reset Password' );
define( 'USER_PASS_RESET_LABEL', 'Email Address' );
define( 'USER_LOGIN_BUTTON_TEXT', 'Login' );
define( 'USER_REGISTER_BUTTON_TEXT', 'Register' );
define( 'USER_REGISTRATION_SUBJECT', SITENAME.' Login Details' );
define( 'LOGGED_IN_SUCCESS', 'Logged in successfully!' );

/* End of file user-config.php */
/* Location: application/plugins/user-system/user-config.php */