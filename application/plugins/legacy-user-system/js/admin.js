$(document).ready(function(){

    //Display normal forms removing disabled class as neede
    $('body').on('click, focus', 'form#admin_users_form input.clearable', function(){
        if($(this).hasClass('disabled')) {
            $(this).removeClass('disabled');
        }
        return false;
    });
    //Display normal forms removing disabled class as neede
    $('body').on('blur', 'form#admin_users_form input.clearable', function(){
        if(!$(this).hasClass('disabled')) {
            $(this).addClass('disabled');
        }
        return false;
    });
    //Add a modified flag to rows so not everything needs to be saved every time
    $('body').on('change', 'form#admin_users_form input, form#admin_users_form select', function(){
        $(this).closest('tr').find('.admin_altered_row').val('true');
    });
    
    //Process the change updates
    //Posts to whichever page submitted the form (in base usage to /users/admin/)
    $('body').on('click', '#admin_save_userupdates', function(){
        var form = $('form#admin_users_form');
        form_data = 'manage_users=true&';
        form_data += form.serialize();
        $.ajax
        ({
            type: "POST", 
            url: siteBase + '/api/admin', 
            data: form_data,
            success: function(data)
            {
                if(data.success == true)
                {
                    $('.admin_user_update_notification').removeClass('admin_error').addClass('admin_success').html(data.message).fadeIn();
                }
                else
                {
                    $('.admin_user_update_notification').removeClass('admin_success').addClass('admin_error').html(data.message).fadeIn();   
                }
            }, 
            error: function(data)
            {
                console.log(data);
            } 
        });
        return false;
    });
    
    //Add new users from admin
    //Posts to whichever page submitted the form (in base usage to /users/admin/)
    $('body').on('click', '#admin_save_newuser', function(){
        var form = $('form#admin_new_user_form');
        form_data = form.serialize();
        $.ajax
        ({
            type: "POST",
            data: form_data, 
            url: siteBase + '/api/admin', 
            success: function(data)
            {
                if(data.success == true)
                {
                    $('.admin_user_add_notification').removeClass('admin_error').addClass('admin_success').html(data.message).fadeIn();
                }
                else
                {
                    $('.admin_user_add_notification').removeClass('admin_success').addClass('admin_error').html(data.message).fadeIn();   
                }
            }, 
            error: function(data)
            {
                console.log(data);
            } 
        });
        return false;
    });

});

/* End of file admin.js */
/* Location: application/plugins/user-system/js/admin.js */