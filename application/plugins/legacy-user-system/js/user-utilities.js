$(document).ready(function(){
    
    //Add new users from admin
    //Posts to whichever page submitted the form (in base usage to /users/admin/)
    $('body').on('click', '#save_userupdates', function(){
        var form = $('form#user_profile_form');
        form_data = form.serialize();
        $.ajax
        ({
            type: "POST",
            data: form_data,
            url: siteBase + '/api/user', 
            success: function(data)
            {
                $('.user_update_notification').show().html(data);
                if(data.success == true)
                {
                    $('.user_update_notification').removeClass('error').addClass('success').html(data.message).fadeIn();
                }
                else
                {
                    $('.user_update_notification').removeClass('success').addClass('error').html(data.message).fadeIn();   
                }
                
                //Password update messages
                if(data.password_update == true)
                {
                    $('.user_update_pass').removeClass('error').addClass('success').html(data.password_message).fadeIn();
                }
                else
                {
                    $('.user_update_pass').removeClass('success').addClass('error').html(data.password_message).fadeIn();
                }
            }, 
            error: function(data)
            {
                console.log(data);
            } 
        });
        return false;
    });

});

/* End of file user utilities.js */
/* Location: application/plugins/user-system/js/user-utiltities.js */