<?php
/**
 * forms-generation.inc.php
 * Provides functions included in user-functions.php to generate default forms
 * User login, registration, forgot password reset
 *
 * @version 1.0
 * @date 27-Sep-2013
 * @package RapidPHPMe-user-system
 **/
 
if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );

/**
 * Generate Registration form
 *
 * @access public
 * @param array nested array
 * @param string button class
 * @param string button text
 * @return string form
 */
if( !function_exists( 'user_registration_form' ) )
{
    function user_registration_form( $crsf = false, $fields = array(), $button_class = 'register', $button_text = USER_REGISTER_BUTTON_TEXT )
    {
        
        /**
         * Default registration form fields
         * Label => array(inputname => input-type)
         */
        $registration_fields = array(
            array( 
                'label' => 'User Name', 
                'type' => 'text', 
                'name' => 'user_name', 
                'class' => 'required'
            ), 
            array(
                'label' => 'Email Address', 
                'type' => 'email', 
                'name' => 'user_email', 
                'class' => 'required'
            ), 
            array( 
                'label' => 'Password', 
                'type' => 'password', 
                'name' => 'user_password', 
                'class' => 'required'
            )
        );
        
        //Optional fields added to original array
        if( !empty( $fields ) )
        {
            $registration_fields = array_merge_recursive( $registration_fields, $fields );  
        }
        
        $build = '<form action="'.USER_DEFAULT_REDIRECT.'" method="post" class="forms">' . PHP_EOL;
        
        //Include CRSF input
        if( $crsf )
        {
            $build .= crsf( false, REG_CRSF_INPUT_NAME ) . PHP_EOL;
        }
        
        foreach( $registration_fields as $field )
        {
            
            //Handle adding field labels
            if( isset( $field['label'] ) && !empty( $field['label'] ) )
            {
                $build .= '<label for="'.$field['name'].'">'.$field['label'].'</label>' . PHP_EOL;
            }
            
            //Handle added classes
            $class = '';
            if( isset( $field['class'] ) && !empty( $field['class'] ) )
            {
                $class = ' class="'.$field['class'].'"';
            }
            
            //Handle prepopulated input values and postbacks
            $value = '';
            if( isset( $field['value'] ) && !empty( $field['value'] ) )
            {
                $value = $field['value'];
            }
            elseif( isset( $_POST[$field['name']] ) && !empty( $_POST[$field['name']] ) )
            {
                $value = stripslashes( $_POST[$field['name']] );
            }
            
            //Text, email, password
            if( $field['type'] == 'text' || $field['type'] == 'email' || $field['type'] == 'password' )
            {
                $build .= '<input type="'.$field['type'].'" name="'.$field['name'].'" value="'.$value.'"'.$class.' />' . PHP_EOL;
            }
            //Textarea
            if( $field['type'] == 'textarea' )
            {
                $build .= '<textarea name="'.$field['name'].'" rows="" cols=""'.$class.'>'.$value.'</textarea>' . PHP_EOL;
            }
            //Hidden
            if( $field['type'] == 'hidden' )
            {
                $build .= '<input type="'.$field['type'].'" name="'.$field['name'].'" value="'.$value.'"'.$class.' />' . PHP_EOL;
            }
        }

        $build .= '<input type="submit" class="'.$button_class.'" name="submit" value="'.$button_text.'" />' . PHP_EOL;

        $build .= '</form>' . PHP_EOL;
        
        echo $build;
    }

}


/**
 * Generate login form
 * Default usage: login_form();
 *
 * @access public
 * @param array nested array
 * @param string button class
 * @param string button text
 * @return string form
 */
if( !function_exists( 'user_login_form' ) )
{

    function user_login_form( $crsf = true, $added_login_fields = array(), $button_class = 'login', $button_text = USER_LOGIN_BUTTON_TEXT )
    {
        
        /**
         * Required login form fields
         * Label => array(inputname => input-type)
         */
        $login_fields = array(
            array( 'label' => 'Email or username', 'type' => 'text', 'name' => 'username', 'class' => 'required' ), 
            array( 'label' => 'Password', 'type' => 'password', 'name' => 'password', 'class' => 'required' ), 
            array( 'type' => 'hidden', 'name' => 'redirect_to', 'value' => !empty( $_GET['redirect'] ) ? $_GET['redirect'] : '' )
        );
        
        //Optional fields added to original array
        if( !empty( $added_login_fields ) )
        {
            $login_fields = array_merge_recursive( $login_fields, $added_login_fields );  
        }
        
        $login_form = '<form action="'.USER_DEFAULT_REDIRECT.'" method="post" id="user_login">' . PHP_EOL;
        
        //Include CRSF input
        if( $crsf )
        {
            $login_form .= crsf( false, LOG_CRSF_INPUT_NAME ) . PHP_EOL;
        }
        
        foreach( $login_fields as $field )
        {
            //Handle adding field labels
            if( isset( $field['label'] ) && !empty( $field['label'] ) )
            {
                $login_form .= '<label for="'.$field['name'].'">'.$field['label'].'</label>' . PHP_EOL;
            }
            
            //Handle added classes
            $class = '';
            if( isset( $field['class'] ) && !empty( $field['class'] ) )
            {
                $class = ' class="'.$field['class'].'"';
            }
            
            //Handle prepopulated input values
            $value = '';
            if( isset( $field['value'] ) && !empty( $field['value'] ) )
            {
                $value = $field['value'];
            }
            
            //Text, email, password
            if( $field['type'] == 'text' || $field['type'] == 'email' || $field['type'] == 'password' )
            {
                $login_form .= '<input type="'.$field['type'].'" name="'.$field['name'].'" value="'.$value.'"'.$class.' />' . PHP_EOL;
            }
            //Textarea
            if( $field['type'] == 'textarea' )
            {
                $login_form .= '<textarea name="'.$field['name'].'" rows="" cols=""'.$class.'>'.$value.'</textarea>' . PHP_EOL;
            }
            //Hidden
            if( $field['type'] == 'hidden' )
            {
                $login_form .= '<input type="'.$field['type'].'" name="'.$field['name'].'" value="'.$value.'"'.$class.' />' . PHP_EOL;
            }

        }

        $login_form .= '<input type="submit" class="'.$button_class.'" value="'.$button_text.'" name="submit" />' . PHP_EOL;
        
        $login_form .= '</form>' . PHP_EOL;
        
        echo $login_form;
    }

}


/**
 * Generate Password Reset form
 *
 * @access public
 * @param string CRSF[true/false]
 * @return string form
 */
if( !function_exists( 'user_password_reset_form' ) )
{

    function user_password_reset_form( $crsf = false )
    {
        
        $build = '<form action="'.USER_DEFAULT_REDIRECT.'" method="post">' . PHP_EOL;
        
        //Include CRSF input
        if( $crsf )
        {
            $build .= crsf( false, RES_CRSF_INPUT_NAME ) . PHP_EOL;
        }
        $build .= '<label for="user_reset_email">'.USER_PASS_RESET_LABEL.'</label>' . PHP_EOL;  
    
        $sub_data = NULL;
        if( isset( $_POST['user_reset_email'] ) )
        {
            $sub_data = stripslashes( $_POST['user_reset_email'] );
        }
            
        $build .= '<input type="text" name="user_reset_email" value="'.$sub_data.'" />' . PHP_EOL;
        $build .= '<input type="submit" name="submit" value="'.USER_PASS_RESET_BUTTON.'" />' . PHP_EOL;
        $build .= '</form>' . PHP_EOL;
        
        echo $build;
    }

}

/* End of file forms-generation.inc.php */
/* Location: application/plugins/user-system/forms/forms-generation.inc.php */