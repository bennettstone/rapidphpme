<?php
/**
 * user-settings.inc.php
 * Provides forms for users to manage their profile settings
 * Included via user-functions.php
 *
 * $user_system = new includeHelper();
 * $user_system->add_plugin( 'user-system/user-functions.php' );
 *
 * @version 1.0
 * @date 27-Sep-2013
 * @package RapidPHPMe-user-system
 **/
global $db;
$user = $db->filter( $_SESSION['user_id'] );

list( $username, $email ) = $db->get_row( "SELECT user_name, AES_DECRYPT(user_email, '".E_ST."') FROM ".USERS." WHERE user_id = '$user' LIMIT 1" );
?>

<form id="user_profile_form" class="user_form" action="" method="post">
    <div class="user_update_notification hidden_area"></div>
    
    <input type="hidden" name="edit_profile" value="true" />
    
    <label>Username</label>
    <input type="text" name="user_name" value="<?php echo $username; ?>" />
    
    <label>Email Address</label>
    <input type="text" name="user_email" value="<?php echo $email; ?>" />
    
    <h3>Change Password</h3>
    <div class="user_update_pass hidden_area"></div>
    
    <label>Current Password</label>
    <input type="password" name="current_password" />
    
    <label>New Password</label>
    <input type="password" name="new_password" />
    
    <p>
        <button name="save" id="save_userupdates">Save Changes</button>
    </p>
</form>

<?php
/* End of file user-settings.inc.php */
/* Location: application/plugins/user-system/forms/user-settings.inc.php */