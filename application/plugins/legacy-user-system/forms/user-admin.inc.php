<?php
/**
 * user-admin.php
 * Provides administrator output of users as well as management of users
 * Included via user-functions.php
 *
 * $user_system = new includeHelper();
 * $user_system->add_plugin( 'user-system/user-functions.php' );
 *
 * Dependencies:
 * /application/core/class.db.php
 * /application/plugins/phpmailer/class.phpmailer.php
 *
 * @version 1.0
 * @date 22-Sep-2013
 * @package RapidPHPMe-user-system
 **/

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );
//Make sure the user is admin
if( !function_exists( 'check_admin' ) || ( function_exists( 'check_admin' ) && !check_admin() ) ) exit( 'No access allowed' );

global $db;
global $admin_user_levels;
?>
<h2>Manage Users</h2>

<form id="admin_users_form" class="admin_user_form" action="" method="post">
    <div class="admin_user_update_notification admin_hide_area"></div>
    <table class="admin-users-table">
        <thead>
            <th>Username</th>
            <th>Email</th>
            <th>Password</th>
            <th>User Level</th>
            <th>Registered</th>
            <th>Last Login</th>
            <th>Num Logins</th>
            <th>Delete</th>
            <th>Ban</th>
        </thead>
        <tbody>
        <?php
        $users = "SELECT user_id, user_level, user_name, AES_DECRYPT(user_email, '".E_ST."') AS email, user_date, last_login, num_logins, banned FROM ".USERS." ORDER BY user_date";
        $all_users = $db->get_results( $users );
        if( !empty( $all_users ) )
        {
            $row_count = 0;
            foreach( $all_users as $user )
            {
                $row_color = ($row_count % 2) ? 'even' : 'odd';
            	$row_count++;
            	
            	//Make sure users cannot accidentally delete themselves
            	$delete_disabled = '';
            	if( $_SESSION['user_id'] == $user['user_id'] )
            	{
            	    $delete_disabled = ' disabled="disabled"';
            	}
            ?>
                <tr class="<?php echo $row_color; ?>">
                    <input type="hidden" name="user_id[]" value="<?php echo $user['user_id']; ?>"<?php echo $delete_disabled; ?> />
                    <td class="admin_hide_area">
                        <input type="text" name="altered[]" class="admin_altered_row" value="false"<?php echo $delete_disabled; ?> />
                    </td>
                    <td>
                        <input type="text" name="user_name[]" value="<?php echo clean( $user['user_name'] ); ?>" class="disabled clearable"<?php echo $delete_disabled; ?> />
                    </td>
                    <td>
                        <input type="text" name="email[]" value="<?php echo clean( $user['email'] ); ?>" class="disabled clearable"<?php echo $delete_disabled; ?> />
                    </td>
                    <td>
                        <input type="text" name="pass[]" value="" class="disabled clearable"<?php echo $delete_disabled; ?> />
                    </td>
                    <td>
                        <select name="user_level[]"<?php echo $delete_disabled; ?>>
                            <?php
                            foreach( $admin_user_levels as $num => $label )
                            {
                                $current = '';
                                if( $user['user_level'] == $num )
                                    $current = ' selected="selected"';
                                echo '<option value="'.$num.'"'.$current.'>'.$label.'</option>'. PHP_EOL;
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" value="<?php echo clean( $user['user_date'] ); ?>" disabled="disabled" />
                    </td>
                    <td>
                        <input type="text" value="<?php echo !empty( $user['last_login'] ) ? clean( $user['last_login'] ) : 'N/A'; ?>" disabled="disabled" />
                    </td>
                    <td>
                        <input type="text" value="<?php echo clean( $user['num_logins'] ); ?>" disabled="disabled" />
                    </td>
                    <td>
                        <input type="checkbox" name="delete[]" id="delete" value="1"<?php echo $delete_disabled; ?> />
                    </td>
                    <td>
                        <input type="checkbox" name="ban[]" id="ban" value="1"<?php echo ( $user['banned'] == '1' ) ? ' checked="checked"' : ''; ?><?php echo $delete_disabled; ?> />
                    </td>
                </tr>
            <?php
            } //end foreach
            ?>
            
            <!--Add save option for the changed values-->
            <tr>
                <td colspan="7" align="right">
                    <button name="save" id="admin_save_userupdates">Save Changes</button>
                </td>
            </tr>
            
        <?php    
        }
        else
        {
            //No users found
            echo '<tr>';
            echo '<td colspan="7">No users located</td>';
            echo '</tr>';
        }
        ?>
        </tbody>
    </table>
</form>


<h2>Add New User</h2>
<form id="admin_new_user_form" class="admin_user_form" action="" method="post">
    <div class="admin_user_add_notification admin_hide_area"></div>
    <input type="hidden" name="new_user" value="true" />
    <label>User Name</label>
    <input type="text" name="user_name" value="" />
    
    <label>Email</label>
    <input type="text" name="user_email" value="" />
    
    <label>Password (leave blank for auto generated)</label>
    <input type="text" name="user_password" value="" />
    
    <label for="">User Level</label>
    <select name="user_level">
        <option value="1"></option>
        <?php
        foreach( $admin_user_levels as $num => $label )
        {
            echo '<option value="'.$num.'">'.$label.'</option>'. PHP_EOL;
        }
        ?>
    </select>
    
    <label>Notify user by email?</label>
    <input type="radio" name="notify_user" value="1" /> Yes<br />
    <input type="radio" name="notify_user" value="0" checked="checked" /> No<br />
    
    <button name="save" id="admin_save_newuser">Add User</button>
    
</form>

<?php
/* End of file user-admin.inc.php */
/* Location: application/plugins/user-system/forms/user-admin.inc.php */