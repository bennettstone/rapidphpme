<?php
/**
 * controller-users.php
 * Load login and registration page
 *
 * @version 1.0
 * @date 21-Sep-2013
 * @package RapidPHPMe
 **/

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );

function controller_users()
{

    //Allow users to logout by going to /users/?logout=true
    if( !empty( $_GET['logout'] ) && $_GET['logout'] == 'true' )
    {
        user_system_logout();
        exit;
    }

    //Start an array of data to send to the header for meta
    $header = array(
        'title' => 'Login or register!', 
        'keywords' => 'awesome, user, system, example', 
        'description' => 'Check out this awesome user system'
    );

    //Start an array of data to send to the view file, not required, you could easily check for isset vs. !empty
    $contents = array(
        'heading' => 'Login or register', 
        'login_error' => '', 
        'registration_error' => '', 
        'registration_success' => '', 
        'reset_error' => '', 
        'reset_success' => '', 
        'activate_error' => '', 
        'activate_success' => ''
    );

    //Process Login submissions
    if( isset($_POST['submit']) && $_POST['submit'] == USER_LOGIN_BUTTON_TEXT )
    {
        $params['email'] = $_POST['username'];
        $params['password'] = $_POST['password'];
        $params['csrf'] = $_POST[LOG_CRSF_INPUT_NAME];
        if( !empty( $_POST['redirect_to'] ) )
        {
            $params['redirect_to'] = $_POST['redirect_to'];
        }

        $login = user_system_login( $params );

        //$login['error'] will only exist if the user doesn't login successfully and get redirected
        $contents['login_error'] = $login['error'];
    }

    //Process registrations
    if( isset( $_POST['submit'] ) && $_POST['submit'] == USER_REGISTER_BUTTON_TEXT )
    {
        $registration_data = array(
            'user_name' => array( 
                'value' => $_POST['user_name'],
                'type' => 'string', 
                'required' => true, 
                'validation' => 'Please enter a user name'
            ),
            'user_email' => array( 
                'value' => $_POST['user_email'],
                'type' => 'email', 
                'required' => true, 
                'validation' => 'A valid email address is required'
            ), 
            'user_pwd' => array( 
                'value' => $_POST['user_password'], 
                'required' => true, 
                'validation' => 'Password is required'
            )
        );
        $registration = user_system_registration( $registration_data );
        if( isset( $registration['error'] ) )
        {
            $contents['registration_error'] = $registration['error'];
        }
        if( isset( $registration['success'] ) )
        {
            $contents['registration_success'] = $registration['success'];
        }
    }

    //Process account activations
    if( !empty( $_GET['user'] ) && is_numeric( $_GET['activ_code'] ) )
    {
        $activation_data = array(
            'user' => $_GET['user'],
            'code' => $_GET['activ_code']
        );
        $activated = user_system_activation( $activation_data );
        $contents['activate_error'] = $activated['error']; 
        $contents['activate_success'] = $activated['success'];
    }

    //Process password resets
    if( isset( $_POST['submit'] ) && $_POST['submit'] == USER_PASS_RESET_BUTTON )
    {
        $reset = user_system_password_reset( $_POST['user_reset_email'] );
        $contents['reset_error'] = $reset['error']; 
        $contents['reset_success'] = $reset['success'];
    }


    //Load /views/layouts/header.php
    view( 'header', $header );

    //Load /views/users.php and pass contents of $contents array
    view( 'users', $contents );

    //Load /views/layouts/footer.php
    view( 'footer' );
}