<?php
/**
 * user-settings.php
 *
 * @version 2.0
 * @date 26-Sep-2013
 * @package RapidPHPMe
 **/

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );
?>

<h2>User Settings</h2>

<?php user_system_user_settings(); ?>