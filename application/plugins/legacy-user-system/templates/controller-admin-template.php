<?php
/**
 * controller-admin.php
 * Load login and registration page
 *
 * @version 1.0
 * @date 21-Sep-2013
 * @package RapidPHPMe
 **/

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );

function controller_admin()
{
    
    //Make sure only logged in users can see the page
    page_protect();
    //Make sure only admins can see this page, else direct them to their user area
    force_admin();

    $header = array(
        'title' => 'Admin only area!'  
    );

    
    //Handle user updates from the users.js script via user-admin.php output
    if( isset( $_POST['manage_users'] ) && $_POST['manage_users'] == 'true' )
    {
        unset( $_POST['manage_users'] );
        echo admin_update_users( $_POST );
        exit;
    }
    
    //Handle new use additions from the users.js script via user-admin.php output
    if( isset( $_POST['new_user'] ) && $_POST['new_user'] == 'true' )
    {
        echo admin_add_user( $_POST );
        exit;
    }
    
    //Load /views/layouts/header.php
    view( 'header', $header );

    //Load /views/users.php and pass contents of $contents array
    view( 'admin' );

    //Load /views/layouts/footer.php
    view( 'footer' );
}