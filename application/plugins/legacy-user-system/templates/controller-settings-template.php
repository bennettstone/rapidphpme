<?php
/**
 * controller-user-settings.php
 * Load user profile edit form for users
 *
 * @version 1.0
 * @date 26-Sep-2013
 * @package RapidPHPMe
 **/

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );

function controller_user_settings()
{
    
    //Make sure only logged in users can see the page
    page_protect();

    $header = array(
        'title' => 'Manage account settings'  
    );

    
    //Handle user updates from the users.js script via user-admin.php output
    if( isset( $_POST['edit_profile'] ) && $_POST['edit_profile'] == 'true' )
    {
        echo user_update_profile( $_POST );
        exit;
    }
    
    //Load /views/layouts/header.php
    view( 'header', $header );

    //Load /views/users.php and pass contents of $contents array
    view( 'user-settings' );

    //Load /views/layouts/footer.php
    view( 'footer' );
}