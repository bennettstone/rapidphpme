<?php
/**
 * user-functions.php
 * Handles user system registration, login, activation, logout, forgot password functionality
 * Outputs login, password reset, and registration forms
 *
 * To include:
 *
 * $user_system = new includeHelper();
 * $user_system->add_plugin( 'user-system/user-functions.php' );
 *
 * Dependencies:
 * /application/core/class.db.php
 * /application/plugins/phpmailer/class.phpmailer.php
 *
 * Facebook login and registration may be included by assigning the FB_ACTIVE to "true"
 * Facebook files are included if FB_ACTIVE is true at the bottom of this file to ensure functional inclusion
 *
 * @version 1.0
 * @date 16-Oct-2012
 * @package RapidPHPMe-user-system
 **/

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );

//Make sure we have access to a database, otherwise just get out
global $db;

//Include configuration options
require( 'user-config.php' );

//Include form generation functions
require_once( 'forms/forms-generation.inc.php' );


/**
 * Validate email addresses
 * @access public
 * @param string email
 * @return bool
 */
function user_valid_email( $address )
{
    if( filter_var( $address, FILTER_VALIDATE_EMAIL ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}


function user_valid_id( $username )
{
    if( preg_match( '/^[a-z\d_]{5,20}$/i', $username ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}


/**
 * Get accurate IP Address of users 
 *
 * @access public
 * @param none
 * @return string
 *
 */
function user_system_get_ip()
{
    if( isset( $_SERVER ) )
    {
        if( isset( $_SERVER["HTTP_X_FORWARDED_FOR"] ) )
        {
            $realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif( isset( $_SERVER["HTTP_CLIENT_IP"] ) )
        {
            $realip = $_SERVER["HTTP_CLIENT_IP"];
        }
        else
        {
            $realip = $_SERVER["REMOTE_ADDR"];
        }
    }
    else
    {
        if( getenv( 'HTTP_X_FORWARDED_FOR' ) )
        {
            $realip = getenv( 'HTTP_X_FORWARDED_FOR' );
        }
        elseif( getenv( 'HTTP_CLIENT_IP' ) )
        {
            $realip = getenv( 'HTTP_CLIENT_IP' );
        }
        else
        {
            $realip = getenv( 'REMOTE_ADDR' );
        }
    }
    return $realip;
}


/**
 * Generate new password for user resets 
 *
 * @access public
 * @param int length
 * @return string
 *
 */
function user_generate_password( $length = 14 )
{
    $password = "";
    $possible = "0123456789bcdfghjkmnpqrstvwxyz"; //no vowels

    $i = 0;

    while( $i < $length )
    {
        $char = substr( $possible, mt_rand( 0, strlen( $possible )-1 ), 1 );

        if( !strstr( $password, $char ) )
        {
            $password .= $char;
            $i++;
        }
    }
    return $password;
}


/**
 * Hash Passwords for storage
 *
 * @access public
 * @param string password
 * @param string salt
 * @return string
 *
 */
function user_system_password_hash( $password, $salt = null )
{   
    $unique_salt = substr( sha1( mt_rand() ), 0, 22 );
    return crypt( $password, '$2a$10$'. $unique_salt );
}


/**
 * Function to evaluate password match between hashed and nonhashed values
 * Example:
 * if( check_password( $pass_hash, $_POST['login-password'] ) )
 * {
 *    echo 'Matched!';
 * }
 *
 * @access public
 * @param $hash db stored hashed password
 * @param $password usually $_POST value from login form
 * @return bool
 */
function user_check_password( $hash, $password )
{
    $full_salt = substr( $hash, 0, 29 );
    $new_hash = crypt( $password, $full_salt );
    if( $hash == $new_hash )
    {
        return true;
    }
    else
    {
        return false;
    }
}   


/**
 * Generate access key
 *
 * @access public
 * @param int
 * @return string
 */
function user_generate_key( $length = 14 )
{
    
    $password = "";
    $possible = "0123456789abcdefghijkmnopqrstuvwxyz";

    $i = 0;
    while( $i < $length )
    {

        $char = substr( $possible, mt_rand( 0, strlen( $possible )-1 ), 1 );

        if( !strstr( $password, $char ) )
        {
            $password .= $char;
            $i++;
        }
    }
    return $password;

}


/**
 * Install the base users table 
 *
 * @access public
 * @param none
 * @return bool [success]
 * @return string [error]
 *
 */
function user_table_install()
{
    global $db;
    if( !$db ) return;
    
    $install = "CREATE TABLE IF NOT EXISTS `".USERS."` (
      `user_id` int(11) NOT NULL AUTO_INCREMENT,
      `user_fb_id` varchar(220) DEFAULT NULL,
      `md5_id` varchar(220) DEFAULT '',
      `user_name` varchar(220) NOT NULL DEFAULT '',
      `user_email` blob NOT NULL,
      `user_level` tinyint(4) NOT NULL DEFAULT '1',
      `user_pwd` varchar(220) NOT NULL DEFAULT '',
      `user_date` timestamp NULL DEFAULT NULL,
      `time` text NOT NULL,
      `users_ip` varchar(200) NOT NULL DEFAULT '',
      `approved` int(1) NOT NULL DEFAULT '0',
      `user_activation_code` int(10) NOT NULL DEFAULT '0',
      `banned` int(1) NOT NULL DEFAULT '0',
      `ckey` varchar(220) NOT NULL DEFAULT '',
      `ctime` varchar(220) NOT NULL DEFAULT '',
      `last_login` timestamp NULL DEFAULT NULL,
      `num_logins` int(11) NOT NULL DEFAULT '0', 
      `user_pic` varchar(220) NULL DEFAULT NULL, 
      PRIMARY KEY (`user_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
    
    $installation = $db->query( $install );
    
    if( $installation )
    {
        return true;
    }
    else
    {
        return 'Failed to install users table.';   
    }
}


/**
 * Intall the lockdown table
 * @access public
 * @param none
 * @return mixed
 */
function lockdown_table_install()
{
    $install = "CREATE TABLE IF NOT EXISTS `".LOCKDOWNS."` (
      `lockdown_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `lockdown_username` varchar(220) DEFAULT NULL,
      `lockdown_ip` varchar(20) DEFAULT NULL,
      `lockdown_timestamp` timestamp NULL DEFAULT NULL,
      `lockdown_count` tinyint(4) DEFAULT NULL,
      PRIMARY KEY (`lockdown_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    
    global $db;
    if( !$db ) return;
    
    $installation = $db->query( $install );
    
    if( $installation )
    {
        return true;
    }
    else
    {
        return 'Failed to install lockdown table.';   
    }
}

//Make sure users table exists
function user_system_confirm_tables()
{
    global $db;
    if( !$db ) return;
    
    if( !$db->table_exists( USERS ) )
    {
        user_table_install();
    }
    if( !$db->table_exists( LOCKDOWNS ) )
    {
        lockdown_table_install();
    }
}
user_system_confirm_tables();


if( !function_exists( 'generate_user' ) )
{
    
    function generate_user()
    {
        global $db;
        if( !$db ) return;
        
        $new_user = array(
            'user_name' => "'admin'", 
            'user_email' => "AES_ENCRYPT('bennettstone@gmail.com', '".E_ST."')", 
            'user_level' => 5, 
            'user_pwd' => "'".user_system_password_hash( '123456' )."'", 
            'user_date' => "'".right_now()."'", 
            'users_ip' => "'".user_system_get_ip()."'", 
            'approved' => 1
        );
        $db->insert_safe( USERS, $new_user );
    }
    
}


/**
 * Function to retrieve current number of lockouts for a given username/ip combination
 * Incoming data is assumed to be sanitized prior to requesting this function
 *
 * @access public
 * @param string $user_email
 * @param string $ip
 * @return int
 */
function get_lockdowns( $user_email, $ip )
{
    if( !USE_LOCKDOWN )
    {
        return false;
    }
    
    global $db;
    if( !$db ) return;
    
    list( $lockouts, $last_id, $time ) = $db->get_row( "SELECT lockdown_count, lockdown_id, lockdown_timestamp FROM ".LOCKDOWNS." WHERE lockdown_username = '$user_email' AND lockdown_ip = '$ip'" );
    
    //Return false if there is no lockout
    if( empty( $lockouts ) && empty( $time ) )
    {
        return false;
    }
    
    if( !empty( $time ) )
    {
        $time = strtotime( USER_LOCKDOWN_TIMEOUT, strtotime( $time ) );
    }
    
    return array( 'count' => $lockouts, 'last_id' => $last_id, 'stamp' => $time );
}


/**
 * Function to log invalid login attempts
 *
 * @access public
 * @param string $user_email
 * @param string $ip
 * @return bool
 */
function log_attempt( $user_email, $reset = false )
{
    //Only do something if lockdowns are enabled
    if( USE_LOCKDOWN )
    {
        global $db;
        if( !$db ) return;

        $this_ip = user_system_get_ip();
        $this_ip = $db->filter( $this_ip );
        $user_email = $db->filter( $user_email );
        
        if( $reset === true )
        {
            $db->delete( LOCKDOWNS, array( 'lockdown_username' => $user_email, 'lockdown_ip' => $this_ip ), 1 );
            return true;
        }

        //Get current number of lockdowns for this username from this IP
        $lockdown_count = get_lockdowns( $user_email, $this_ip );
        
        $insert = array(
            'lockdown_username' => $user_email, 
            'lockdown_ip' => $this_ip, 
            'lockdown_timestamp' => right_now(), 
            'lockdown_count' => ( $lockdown_count['count'] + 1 )
        );
        
        if( $lockdown_count['count'] < 1 )
        {
            $db->insert( LOCKDOWNS, $insert );
        }
        else
        {
            $db->update( LOCKDOWNS, $insert, array( 'lockdown_id' => $lockdown_count['last_id'] ) );
        }
        
    } //end if( USE_LOCKDOWN )
}


/**
 * Function to update user password
 * Compares existing password to user entered existing pass for match
 * Match allows update, no match returns false
 * @access public
 * @param int $user_id
 * @param string $old_password
 * @param string $new_password
 * @return bool
 */
function user_change_password( $user_id, $old_password, $new_password )
{
    global $db;
    if( !$db ) return;
    
    $user_id = $db->filter( $user_id );
    $old_password = $db->filter( $old_password );
    $new_password = $db->filter( $new_password );
    
    list( $stored_password ) = $db->get_row( "SELECT user_pwd FROM ".USERS." WHERE user_id = '$user_id' LIMIT 1" );

    //No pass for some reason.  Fail
    if( empty( $stored_password ) )
    {
        return false;
    }
    
    //Make sure the old password matches the new one before continuing
    if( user_check_password( $stored_password, $old_password ) )
    {
        $new_password = user_system_password_hash( $new_password );
        $update = array( 'user_pwd' => $new_password );
        $update_where = array( 'user_id' => $user_id );
        if( $db->update( USERS, $update, $update_where, 1 ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
    
}


/**
 * Run check to see if user or value exists 
 *
 * Usage:
 * $check_user = array(
 *    'user_email' => 'bennettstone+3lkhadsf@gmail.com', 
 *    'user_id' => 48
 * );
 * $exists = user_exists( 'user_id', $check_user );
 *
 * @access public
 * @param string field to check (i.e. 'user_id' or COUNT(user_id))
 * @param array column name => column value to match
 * @return bool
 *
 */
function user_exists( $check_val = 'user_id', $params = array() )
{
    global $db;
    if( !$db ) return;
    
    $check = array();
    foreach( $params as $field => $value )
    {
        if( !empty( $field ) && !empty( $value ) )
        {
            if( preg_match( '/AES_DECRYPT/i', $value ) || preg_match( '/AES_ENCRYPT/i', $value ) || preg_match( '/now()/i', $value ) )
            {
                $check[] = $db->filter( $field ) ." = ". $value;
            }
            else
            {
                $check[] = $db->filter( $field ) ." = '". $db->filter( $value ) ."'";
            }
        }
    }
    $exist_check = implode( ' AND ', $check );

    $rs_check = "SELECT $check_val FROM ".USERS." WHERE $exist_check";
    $number = $db->num_rows( $rs_check );
    if( $number === 0 )
    {
        return false;
    }
    else
    {
        return true;
    }
}


/**
 * User registration function
 * Usage:
 *
 * if( isset( $_POST['submit'] ) && $_POST['submit'] == USER_REGISTER_BUTTON_TEXT )
 * {
 *     $registration_data = array(
 *         'user_name' => array( 
 *             'value' => $_POST['user_name'],
 *             'type' => 'string', 
 *             'required' => true, 
 *             'validation' => 'Please enter a user name'
 *         ),
 *         'user_email' => array( 
 *             'value' => $_POST['user_email'],
 *             'type' => 'email', 
 *             'required' => true, 
 *             'validation' => 'A valid email address is required'
 *         ), 
 *         'user_pwd' => array( 
 *             'value' => $_POST['user_password'], 
 *             'required' => true, 
 *             'validation' => 'Password is required'
 *         )
 *     );
 *     $registration = user_system_registration( $registration_data );
 *     if( isset( $registration['error'] ) )
 *     {
 *         $contents['registration_error'] = $registration['error'];
 *     }
 *     if( isset( $registration['success'] ) )
 *     {
 *         $contents['registration_success'] = $registration['success'];
 *     }
 * }
 *
 * @access public
 * @param array field_name => array value, type, required [true/false], validation text
 * @return array errors
 * @return array messages
 * @return string header redirect
 */
function user_system_registration( $params = NULL )
{
    //Connect to the database
    global $db;
    if( !$db ) return;
    
    global $config;

    //Start the error collection array
    $errors = array();

    if( empty( $params ) || !is_array( $params ) )
    {
        $errors[] = EMPTY_REGISTRATION;
    }

    //Build the array of valid values and their respective columns
    $register = array();
    //Build array of messages
    $messages = array();

    //Loop through form submission values and check required values
    foreach( $params as $field => $values )
    {   
        //If the 'field' parameter is specified, map the input to that, otherwise fallback to input name
        $database_field = empty( $values['field'] ) ? trim( $field ) : trim( $values['field'] );
        
        //If the user is using crsf-prevention.php functions, ensure system security
        if( $field == REG_CRSF_INPUT_NAME && !check_crsf( $values['value'], REG_CRSF_INPUT_NAME ) )
        {
            $errors[] = INVALID_TOKEN;
        }
    
        if( !empty( $values['required'] ) && $values['required'] === true && empty( $values['value'] ) )
        {
            if( !empty( $values['validation'] ) )
            {
                $errors[] = $values['validation'];
            }
            else
            {
                $errors[] = REQUIRED_FIELD_VALIDATION;
            }
        }
        else
        {
            $input_value = trim( $values['value'] );
            if( !empty( $values['type'] ) )
            {
                switch( $values['type'] )
                {
                
                    case 'string':
                        if( !is_string( $input_value ) )
                            $errors[] = $input_value . ' must be a string.';
                        else
                            $register[$database_field] = $input_value;
                    break;
                    case 'bool':
                        if( $input_value !== true || $input_value !== false )
                            $errors[] = $input_value . ' must be a boolean.';
                        else
                            $register[$database_field] = $input_value;
                    break;
                    case 'int':
                        if( !is_int( $input_value ) )
                            $errors[] = $input_value . ' must be an integer.';
                        else
                            $register[$database_field] = $input_value;
                    break;
                    case 'float':
                        if( !is_float( $input_value ) )
                            $errors[] = $input_value . ' must be a float.';
                        else
                            $register[$database_field] = $input_value;
                    break;
                    case 'email':
                        if( !user_valid_email( $input_value ) )
                            $errors[] = '&quot;' . $input_value . '&quot; must be a valid email.';
                        else
                            $register[$database_field] = $input_value;
                    break;

                } //end switch
            
            } //end type !empty
            else
            {
                $register[$database_field] = $input_value;
            }
            if( !empty( $values['length'] ) )
            {
                //Validate for length
                if( strlen( $values['value'] ) < $values['length'] )
                    $errors[] = $field .' must be at least '. $values['length'].' characters long.';
                else
                    $register[$database_field] = $input_value;
            }
        
        } //end !empty value
        
    
    } //end outer foreach

    //Remove the CRSF submission if it exists
    if( isset( $register[REG_CRSF_INPUT_NAME] ) )
    {
        unset( $register[REG_CRSF_INPUT_NAME] );
    }
    
    
    if( empty( $register['user_pwd'] ) && empty( $register['is_facebook'] ) )
    {
        $errors[] = USER_NO_PASSWORD;
    }
    if( !empty( $register['is_facebook'] ) && empty( $register['user_fb_id'] ) )
    {
        $errors[] = USER_NO_PASSWORD;
    }
    
    if( empty( $register['user_email'] ) )
    {
        $errors[] = USER_NO_EMAIL;
    }    

    if( empty( $errors ) )
    {
        /*
        if( isset( $register['user_fb_id'] ) )
        {
            $send_password = '';
        }
        else
        {
            */
            $send_password = $register['user_pwd'];
            $register['user_pwd'] = user_system_password_hash( $register['user_pwd'] );
        //}
        //Build variable params
        $user_ip = user_system_get_ip();
        
        $activ_code = rand( 1000,9999 );
        $user_email = $register['user_email'];

        //Check for duplicate and existing users
        $total = user_exists( 'user_id', array( 'user_email' => "AES_ENCRYPT('".$db->filter( $user_email ) ."', '".E_ST."')" ) );
        if( $total > 0 )
        {
            $errors['success'] = false;
            $errors['message'] = USER_ALREADY_EXISTS;
            return $errors;
            exit;
        }
        else
        {
        
            //Add user reg date
            $register['user_date'] = right_now();
            $register['user_activation_code'] = $activ_code;
            $register['users_ip'] = $user_ip;
            $entered_username = $db->filter( $register['user_name'] );
            
            //Run through current reg fields and encapsulate them all EXCEPT email...
            foreach( $register as $name => $value )
            {
                $register[$name] = "'". $db->filter( $value )."'";
            }
            
            //Reassign user email to encrypted value and overwrite previous ones
            $safemail = $db->filter( $user_email );
            $register['user_email'] = "AES_ENCRYPT('$safemail', '".E_ST."')";

            //Since we know that all fields have been appropriately encapsulated and sanitized, we'll use the safe insert
            $add_user = $db->insert_safe( USERS, $register );
            
            if( $add_user === true )
            {
                
                $last_id = $db->lastid();
                $md5_id = md5( $last_id );

                $updated = array(
                    'md5_id' => $md5_id
                );
                if( !USER_REQUIRE_ACTIVATION )
                {
                    $updated['approved'] = 1;
                }
                //Add the WHERE clauses
                $where_clause = array(
                    'user_id' => $last_id
                );
                $updated = $db->update( USERS, $updated, $where_clause );  
            
                if( $updated )
                {   
                    //If user acct requires activation, send link to activate
                    $a_link = NULL;
                    if( USER_REQUIRE_ACTIVATION && !isset( $register['user_fb_id'] ) )
                    {
                        $a_link = '<p><a href="' . USER_ACTIVATION_BASE . $md5_id . '/' . $activ_code . '">Click here to activate your account.</a></p>';
                    }

                    $message = "<p>Thank you for registering with ".SITENAME."!</p>";
                    $message .= "<p>Here are your login details<br />";
                    $message .= "Username: ". $entered_username."<br />";
                    $message .= "User Email: ". $user_email."<br />";
                    $message .= "Password: ".$send_password."</p>";
                    $message .= $a_link;
                    $message .= "<p>Thank You,<br />";
                    $message .= "Administrator<br />";
                    $message .= "<a href=\"".BASE."\">".SITENAME ."</a>";
                    
                    if( function_exists( 'send_message' ) )
                    {
                        $from = array( 'from' => USER_EMAIL_REPLYTO, 'name' => SITENAME );
                        $m = send_message( $from, $user_email, USER_REGISTRATION_SUBJECT, $message );
                    }
                    else
                    {
                        /**
                         * Fallback in the event that the phpmailer plugin isn't initated
                         */
                        $headers  = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                        $headers .= 'From: '.SITENAME.' System <'.USER_EMAIL_REPLYTO.'>' . "\r\n";
                        $m = mail( $user_email, USER_REGISTRATION_SUBJECT, $message, $headers );
                    }

                    //Display success or error messages
                    if( $m )
                    {
                        if( USER_REQUIRE_ACTIVATION )
                        {
                            $messages['success'] = true;
                            $messages['id'] = $last_id;
                            $messages['message'] = 'Your registration was successful, and an activation link has been sent to '.$user_email;
                        }
                        else
                        {
                            $messages['success'] = true;
                            $messages['id'] = $last_id;
                            $messages['message'] = 'Registration Successful!';   
                        }
                    }
                    else
                    {
                        $messages['success'] = false;
                        $messages['message'] = 'An error has occured.';
                    }
                    return $messages;
                }
                else
                {
                    return ( array( 'success' => false, 'message' => 'An error has occurred.' ) );
                }
            }

        }
    
    }
    else
    {
        return ( array( 'success' => false, 'message' => implode( ', ', $errors ) ) );
    }
}



/**
 * Function to set user cookie for remember me
 * Used by user_system_login()
 *
 * @access public
 * @param int $user_id
 * @param int $key
 * @return none
 */
function set_remember( $user_id, $key )
{
    setcookie( "user_id", $user_id, time()+60*60*24*COOKIE_TIME_OUT, "/" );
    setcookie( "user_key", sha1( $key ), time()+60*60*24*COOKIE_TIME_OUT, "/" );
}


/**
 * Function to validate user remember me cookies
 * Code intentionally separated for legibility
 * Used by page_protect()
 *
 * @access public
 * @param none
 * @return bool
 */
function validate_user_cookie()
{
    global $db;
    if( !$db ) return;
    
    //Validate the user id and user key for intvals and existence
    if( !isset( $_COOKIE['user_id'] ) || !isset( $_COOKIE['user_key'] ) || ( isset( $_COOKIE['user_id'] ) && !is_numeric( $_COOKIE['user_id'] ) ) )
    {
        return false;
    }

    $user = $db->filter( $_COOKIE['user_id'] );
    $key = $db->filter( $_COOKIE['user_key'] );

    list( $ckey, $ctime ) = $db->get_row( "SELECT ckey, ctime FROM ".USERS." WHERE user_id = '$user' LIMIT 1" );

    //Boot if the expiry exceeds default
    if( isset( $ctime ) && (time() - $ctime) > 60*60*24*COOKIE_TIME_OUT )
    {
        return false;
    }

    //If the key IS NOT the key, boot
    if( $key != sha1( $ckey ) )
    {
        return false;
    }

    //If we made it this far, return true
    return true;
}


/**
 * User login function
 * Usage:
 *
 * if( isset($_POST['login']) )
 * {
 *      $user = array( 'email' => $_POST['username'], 'password' => $_POST['password'], 'csrf' => $_POST['data_token'] );
 *      user_system_login( $user );
 *      exit();
 *  }
 *
 * @access public
 * @param array email, password, csrf
 * @return mixed error message OR header redirect
 */
function user_system_login( $params = array() )
{

    //If the user is using crsf-prevention.php functions, ensure system security
    if( empty( $params ) || ( !empty( $params['csrf'] ) && !check_crsf( $params['csrf'], LOG_CRSF_INPUT_NAME ) ) )
    {
        return array( 'success' => false, 'message' => USER_INVALID_LOGIN .' at '.__LINE__ .' in file '. __FILE__  );
    }

    //Initiate the db class
    global $db;
    if( !$db ) return;

    //Fiter the vars, password may not exist due to facebook logins
    $user_email = $db->filter( strtolower( $params['email'] ) );
    $pass = !empty( $params['password'] ) ? $db->filter( $params['password'] ) : '';
    $remember = ( isset( $params['remember'] ) && !empty( $params['remember'] ) ) ? true : false;
    
    //Find out if the user has exceeded login attempts, if so, epic fail
    $lockdowns = get_lockdowns( $user_email, $db->filter( user_system_get_ip() ) );
    if( $lockdowns && ( $lockdowns['count'] >= USER_MAX_LOCKDOWNS ) && ( time() < $lockdowns['stamp'] ) )
    {
        return array( 'success' => false, 'message' => USER_LOCKOUT );
    }

    //Login with username
    if( strpos( $user_email,'@' ) === false )
    {            
        $user_cond = "LOWER(user_name) = '$user_email'";
        $user_data = array( 'LOWER(user_name)' => $user_email );
    }
    //Login with facebook
    elseif( isset( $params['is_facebook'] ) && isset( $params['fb_id'] ) && !empty( $params['fb_id'] ) )
    {
        $user_cond = "LOWER(user_email) = AES_ENCRYPT('$user_email', '".E_ST."') AND user_fb_id = '".$db->filter( $params['fb_id'] )."'";
        $user_data = array( 'LOWER(user_email)' => "AES_ENCRYPT('$user_email', '".E_ST."')", 'user_fb_id' => $db->filter( $params['fb_id'] ) );
    }
    //Login with email
    else
    {
        $user_cond = "LOWER(user_email) = AES_ENCRYPT('$user_email', '".E_ST."')";
        $user_data = array( 'LOWER(user_email)' => "AES_ENCRYPT('$user_email', '".E_ST."')" );
    }

    $total = user_exists( 'user_id', $user_data ); 

    //If the user doesn't exist based on user_exists query return false.
    if( !$total )
    {
        //Log this attempt
        log_attempt( $user_email );
        
        return array( 'success' => false, 'message' => USER_LOGIN_INVALID_USERNAME );
    }

    $result = "SELECT user_id, user_fb_id, user_pwd, approved, user_level, num_logins FROM ".USERS." WHERE $user_cond AND banned = '0'";

    list( $id, $fb_id, $pwd, $approved, $user_level, $num_logins ) = $db->get_row( $result );

    //User hasn't activated their account
    if( !$approved && ( !isset( $params['is_facebook'] ) ) )
    {        
        return array( 'success' => false, 'message' => USER_LOGIN_INACTIVE );   
    }

    //check against salt
    $valid = false;
    if( !empty( $pwd ) && user_check_password( $pwd, $pass ) )
    {
        $valid = true;
    }
    if( isset( $params['is_facebook'] ) && !$params['is_facebook'] && !empty( $params['fb_id'] ) && !empty( $fb_id ) && $fb_id == $params['fb_id'] )
    {
        $valid = true; 
    }

    //No password match, return error
    if( !$valid )
    {
        //Log the attempt as needed
        log_attempt( $user_email );
        
        return array( 'success' => false, 'message' => USER_LOGIN_INVALID_PASS );
    }


    // this sets session and logs user in
    session_regenerate_id( true ); //prevent against session fixation attacks.

    //update the timestamp and key for sessionvars
    $stamp = time();
    $ckey = user_generate_key();
    $now = right_now();

    // this sets variables in the session
    $_SESSION = array(
        'user_id' => $id, 
        'user_level' => $user_level, 
        'HTTP_USER_AGENT' => md5( $_SERVER['HTTP_USER_AGENT'] ), 
        'fpckey' => $ckey, 
        'fpstamp' => $stamp
    );

    $update = array(
        'ctime' => $stamp, 
        'ckey' => $ckey, 
        'num_logins' => ( $num_logins + 1 ), 
        'last_login' => $now
    );
    $updated = $db->update( USERS, $update, array( 'user_id' => $id ) );
    
    /**
     * Handle cookies
     */
    if( $remember )
    {
        set_remember( $_SESSION['user_id'], $ckey );
    }

    //DB error, unexpected, but possible
    if( !$updated )
    {
        return array( 'success' => false, 'message' => USER_INVALID_LOGIN );   
    }

    //Clear any lockouts for this user from this IP
    log_attempt( $user_email, true );
    
    //Redirect the user
    if( !empty( $params['redirect_to'] ) )
    {
        $data = array(
            'success' => true, 
            'message' => 'Logged in successfully!', 
            'redirect' => $params['redirect_to']
        );
        
        //Return data for ajax, so comment this out if using ajax, otherwise keep "header()"
        //return $data;
        header( "Location: " . $params['redirect_to'] );
        exit;
    }
    else
    {
        $data = array(
            'success' => true, 
            'message' => 'Logged in successfully!', 
            'redirect' => USER_DEFAULT_MAIN
        );
        //Return data for ajax, so comment this out if using ajax, otherwise keep "header()"
        //return $data;
        
        header( "Location: " . USER_DEFAULT_MAIN );
        exit;
    }

} //end function


/**
 * User activation function
 * Usage:
 *
 * if( !empty( $_GET['action'] ) && $_GET['action'] == 'activate' && !empty( $_GET['user'] ) && is_numeric( $_GET['activ_code'] ) )
 * {
 *  
 *      $activation_data = array(
 *          'user' => $_GET['user'],
 *          'code' => $_GET['activ_code']
 *       );
 *      $activated = user_system_activation( $activation_data );
 *      $content['activation'] = $activated;
 *  }
 *
 * @access public
 * @param array
 * @return array messages
 */
function user_system_activation( $variables = array() )
{
    if( is_array( $variables ) )
    {
        extract( $variables );
    }
    else
    {
        return array( 
            'success' => false, 
            'message' => USER_INVALID_ACTIVATION
        );
    }

    //Initiate the db
    global $db;
    if( !$db ) return;
    
    //Filter the vars
    $user = $db->filter( $user );
    $code = $db->filter( $code );

    //check if activ code matches and user is valid
    $total = user_exists( 'user_id', array( 'md5_id' => $user, 'user_activation_code' => $code ) ); 
    if( $total === false )
    {
        return array(
            'success' => false, 
            'message' => USER_ACTIVATE_NOT_FOUND
        );
    }
    else
    {
        $update = array(
            'approved' => '1'
        );
        $where_clause = array(
            'md5_id' => $user, 
            'user_activation_code' => $code
        );
        $updated = $db->update( USERS, $update, $where_clause );
        if( $updated === true )
        {
            return array( 'success' => true, 'message' => USER_ACTIVATE_SUCCESS );
        }
        
    } //endelse

} //end user_system_activation()



/**
 * Reset user passwords 
 *
 * @access public
 * @param string email
 * @return string message
 *
 */
function user_system_password_reset( $email = '', $crsf = '', $extra_msg = '' )
{
    
    global $config;
    global $db;
    if( !$db ) return;
    
    $return = array(
        'success' => '', 
        'message' => ''
    );
    
    if( empty( $email ) || !user_valid_email( $email ) )
    {
        return array(
            'message' => USER_RESET_NO_EMAIL, 
            'success' => false
        );
    }
    /*
    if( !empty( $crsf ) && !check_crsf( $crsf, RES_CRSF_INPUT_NAME ) )
    {
        return INVALID_TOKEN;
        exit;
    }
    */
    $email = $db->filter( $email );
    
    $number = user_exists( 'user_id', array( 'user_email' => "AES_ENCRYPT('$email', '".E_ST."')" ) ); 
    if( $number === false )
    {
        return array(
            'message' => USER_ACTIVATE_NOT_FOUND, 
            'success' => false
        );
    }
    
    //User found, reset the password and email it
    $new_pwd = user_generate_password();
    $pwd_reset = user_system_password_hash( $new_pwd );
    
    list( $user_id ) = $db->get_row( "SELECT user_id FROM ". USERS ." WHERE user_email = AES_ENCRYPT('$email', '".E_ST."')" );
    
    $updated = $db->update( USERS, array( 'user_pwd' => $pwd_reset ), array( 'user_id' => $user_id), 1 );
    if( $updated === true )
    {
        $message = '<p>';
        
        if( !empty( $extra_msg ) )
            $message .= $extra_msg . '<br />';
        else
            $message .= 'Here are your new password details ...<br />';
        $message .= "
         User Email: $email<br />
         Passwd: $new_pwd</p>

         <p>Thank You,<br />
         Administrator<br />"
         .SITENAME ."</p>";

        $subject = SITENAME.' Password Reset';
        
        if( function_exists( 'send_message' ) )
        {
            $from = array( 'from' => USER_EMAIL_REPLYTO, 'name' => SITENAME );
            $m = send_message( $from, $email, $subject, $message );  
        }
        else
        {
            /**
             * Fallback in the event that the plugin isn't initated
             */
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: '.SITENAME.' System <'.$config['noreply_email'].'>' . "\r\n";
            $m = mail( $email, $subject, $message, $headers );
        }

        //Display success or error messages
        if( $m )
        {
            return array(
                'success' => true, 
                'message' => USER_PASSWORD_RESET
            );
        }
        else
        {
            return array(
                'message' => 'An error has occurred', 
                'success' => false
            );
        } 
    
    } //end $updated === true
    else
    {
        return array(
            'message' => 'An error has occurred', 
            'success' => false
        );
        
    } //endelse
    
} //end user_system_password_reset()



/**
 * Add admin specific js to the output of admin pages as initiated by user_system_admin_page
 * @access public
 * @param none
 * @return string
 */
function user_system_admin_page_js()
{
    echo '<script type="text/javascript" src="'.USER_PATH.'/js/admin.js"></script>' . PHP_EOL;
}


/**
 * Add admin specific css to the output of admin pages as initiated by user_system_admin_page
 * @access public
 * @param none
 * @return string
 */
function user_system_admin_page_css()
{
    echo '<link rel="stylesheet" href="'.USER_PATH.'/styles/admins.css" />' . PHP_EOL;
}


/**
 * Add user specific css to the output of user settings pages as initiated by user_system_user_settings
 * @access public
 * @param none
 * @return string
 */
function user_system_page_css()
{
    echo '<link rel="stylesheet" href="'.USER_PATH.'/styles/user-settings.css" />' . PHP_EOL;
}


/**
 * Add user specific js to the output of user settings pages as initiated by user_system_user_settings
 * @access public
 * @param none
 * @return string
 */
function user_system_page_js()
{
    echo '<script type="text/javascript" src="'.USER_PATH.'/js/user-utilities.js"></script>' . PHP_EOL;
}


/**
 * Function to output full user management for admins by only calling user_system_admin_page() on a page
 * Controller template provided in templates/controller-admin-template.php
 * View template provided in templates/admin-view-template.php
 * Example route for /core/routes.php:
 * $routes['admin'] = array( 'users', 'admin', ':action', ':id' );
 *
 * @access public
 * @param none
 * @return string
 */
function user_system_admin_page()
{
    //If the user isn't an admin, just return nothing but no forced exit
    if( !check_admin() )
    {
        return '';
    }
    
    //Include the file that has the lengthy forms
    require_once( dirname( __FILE__ ) . '/forms/user-admin.inc.php' );

}


/**
 * Function to generate user settings page by calling user_system_user_settings()
 * Controller template provided in templates/controller-settings-template.php
 * View template provided in templates/user-settings-template.php
 * Example route for /core/routes.php:
 * $routes['user_settings'] = array( 'users', 'settings', ':action' );
 * @access public
 * @param none
 * @return string
 */
function user_system_user_settings()
{
    //If the user isn't logged in but can still get to parent page for some reason, show nothing
    if( !logged_in() )
    {
        return '';
    }
    
    //Include the file that has the lengthy forms
    require_once( dirname( __FILE__ ) . '/forms/user-settings.inc.php' );
}


/**
 * Function to update user profiles by users
 * Saves default values from user_table_install
 * Saved via controllers/users/controller-user-settings.php
 * Saved via this directory user-utilities.js
 * Processes user profile updates, THEN processes password updates
 * Password updates are returned separately
 *
 * Example usage:
 * if( isset( $_POST['edit_profile'] ) && $_POST['edit_profile'] == 'true' )
 * {
 *      echo user_update_profile( $_POST );
 *      exit;
 * }
 */
function user_update_profile( $data )
{

    global $db;
    if( !$db ) return;
    
    if( empty( $data ) || !logged_in() || !$db )
    {
        return '';
    }
    
    $output = array();
    
    $user_id = $db->filter( $_SESSION['user_id'] );
    $params = array();
    
    //Process and validate usernames
    if( isset( $data['user_name'] ) && !empty( $data['user_name'] ) )
    {
        if( !user_valid_id( $data['user_name'] ) )
        {
            $output['success'] = false;
            $output['message'] = 'Please enter a valid username';
            return $output;
        }
        else
        {
            $params[] = "user_name = '". $db->filter( $data['user_name'] ) ."'";   
        }
    }
    
    //Update user fullame
    if( isset( $data['user_fullname'] ) && !empty( $data['user_fullname'] ) )
    {
        $params[] = "user_fullname = '".$db->filter( $data['user_fullname'] ). "'";
    }
    
    //User bio
    if( isset( $data['user_bio'] ) )
    {
        $params[] = "user_bio = '".$db->filter( $data['user_bio'] ). "'";
    }
    
    //User website
    if( isset( $data['user_website'] ) )
    {
        $params[] = "user_website = '".$db->filter( $data['user_website'] )."'";
    }
    
    //Process and validate user email addresses
    if( isset( $data['user_email'] ) && !empty( $data['user_email'] ) )
    {
        if( !user_valid_email( $data['user_email'] ) )
        {
            $output['success'] = false;
            $output['message'] = 'Please enter a valid email';
            return $output;
        }
        else
        {
            $params[] = "user_email = AES_ENCRYPT('".$db->filter( $data['user_email'] ) ."', '".E_ST."')";   
        }
    }
    
    //Handle file uploads for user profile pics
    if( isset( $data['photo'] ) && !empty( $data['photo']['name'] ) && !empty( $data['photo']['tmp_name'] ) )
    {

        $uploaded_type = strtolower( $data['photo']['type'] );
        $allowed = array( 'image/jpeg', 'image/gif', 'image/png', 'image/jpg' );
        if( !in_array( $uploaded_type, $allowed ) )
        {
            $output['success'] = false;
            $output['message'] = 'Only gif, jpg, or png files please';
            return $output;
        }
        
        list( $width, $height ) = getimagesize( $data['photo']['tmp_name'] );
        if( $width > 3000 || $height > 3000 )
        {
            $output['success'] = false;
            $output['message'] = 'Images must be less than 3000px X 3000px';
            return $output;
        }
        
        //Find out if there is a current graphic, if so, delete
        list( $current_image ) = $db->get_row( "SELECT user_pic FROM ".USERS." WHERE user_id = '$user_id' LIMIT 1" );
        if( !empty( $current_image ) )
        {
            $old = ROOT . 'assets/img/user-images/'. $user_id . '/'. $current_image;
            if( file_exists( $old ) )
            {
                chmod( $old, 0777 );
                unlink( $old );
            }
        }

        /**
         * Updated filename and present location for upload
         */
        $end_filename = generate_filename( $data['photo']['name'] );

        $uploaded_file = ROOT . 'assets/img/user-images/'. $user_id .'/'. $end_filename;
        $upload = move_uploaded_file( $data['photo']['tmp_name'], $uploaded_file );

        if( $upload )
        {

            $include = new includeHelper();
            $include->add_helper( 'Zebra_Image.php' );
            $img = new Zebra_Image();
            $img->source_path = $uploaded_file;
            $img->target_path = $uploaded_file;
            $img->resize( 612, 612, ZEBRA_IMAGE_CROP_CENTER );
            
            $params[] = "user_pic = '".$db->filter( $end_filename ) ."'"; 
        }
    }
    elseif( isset( $data['photo'] ) && empty( $data['photo'] ) )
    {
        $params[] = "user_pic = ''"; 
    }
    //End user pic upload
    
    
    //Made it this far, update the user
    $query = "UPDATE " . USERS . " SET ";
    $query .= implode( ', ', $params );
    $query .= " WHERE user_id = '$user_id' LIMIT 1";
    if( $db->query( $query ) )
    {
        $output['success'] = true;
        $output['message'] = 'Profile updated successfully!';
    }
    else
    {
        $output['success'] = false;
        $output['message'] = 'An error has occurred';
    }
    
    //Process password updates separately
    if( isset( $data['current_password'] ) && !empty( $data['current_password'] ) && isset( $data['new_password'] ) && !empty( $data['new_password'] ) )
    {
        //Make sure the old password and new password are valid, and old password is what currently exists
        if( user_change_password( $user_id, $db->filter( $data['current_password'] ), $db->filter( $data['new_password'] ) ) )
        {
            $output['password_update'] = true;
            $output['password_message'] = 'Password updated successfully!';
        }
        else
        {
            $output['password_update'] = false;
            $output['password_message'] = 'Your old password is incorrect';
        }
    }
    
    return $output;
}


/**
 * Function to update groups of users at the same time by clicking "Save" button
 * Only available to admins, multiple admin checks are performed prior to any save action
 * Saves default db values from user_table_install
 * Form fields saved are located in this directory in user-admin.php
 * Saved via controllers/admin/controller-admin.php
 * Saved via this directory users.js
 * Example usage:
 * if( isset( $_POST['manage_users'] ) && $_POST['manage_users'] == 'true' )
 * {
 *    echo admin_update_users( $_POST );
 *    exit;
 * }
 *
 * @access public
 * @param array
 * @return array (json)
 */
function admin_update_users( $data )
{
    global $db;
    if( !$db ) return;
    
    if( !check_admin() || !$db )
    {
        return '';
    }

    header( 'Content-type: application/json' );
    $output = array();
    if( !empty( $data ) && is_array( $data ) )
    {
        $updated = 0;
        $output['removals'] = '';
        foreach( $data['user_id'] as $key => $params )
        {
            
            //If the user ID is empty, or the row hasn't been altered, just continue on
            if( !isset( $data['user_id'][$key] ) || !isset( $data['altered'][$key] ) || ( isset( $data['altered'][$key] ) && $data['altered'][$key] != 'true' ) )
            {
                continue;
            }
            else
            {
                //Assign the user ID to a var
                $user_id = $db->filter( $data['user_id'][$key] );

                //Handle deletions then continue, skipping all the updates
                if( isset( $data['delete'][$key] ) && !empty( $data['delete'][$key] ) )
                {
                    $db->delete( USERS, array( 'user_id' => $user_id ), 1 );
                    $output['removals'] .= $user_id .',';
                    continue;
                }

                //Start the update values array
                $update = array();

                //Username
                if( isset( $data['user_name'][$key] ) && !empty( $data['user_name'][$key] ) )
                {
                    $update[] = "user_name = '".$db->filter( $data['user_name'][$key] )."'";
                }
                //User email
                if( isset( $data['email'][$key] ) && !empty( $data['email'][$key] ) )
                {
                    $update[] = "user_email = AES_ENCRYPT('".$db->filter( $data['email'][$key] )."', '".E_ST."')";
                }
                //User password
                if( isset( $data['pass'][$key] ) && !empty( $data['pass'][$key] ) )
                {
                    $update[] = "user_pwd = '".user_system_password_hash( $db->filter( $data['pass'][$key] ) )."'";
                }
                //User level
                if( isset( $data['user_level'][$key] ) && !empty( $data['user_level'][$key] ) )
                {
                    $update[] = "user_level = '".$db->filter( $data['user_level'][$key] )."'";
                }
                //Banned
                if( isset( $data['ban'][$key] ) && !empty( $data['ban'][$key] ) )
                {
                    $update[] = "banned = 1";
                }
                else
                {
                    $update[] = "banned = 0";
                }

                //Only run the update query if there is data to update for this user
                if( !empty( $update ) )
                {
                    $query = "UPDATE " . USERS . " SET ";
                    $query .= implode( ', ', $update );
                    $query .= " WHERE user_id = '$user_id'";
                    if( $db->query( $query ) )
                    {
                        $updated++;
                    }
                }
            }
            
        }
        if( $updated > 0 )
        {
            $output['success'] = true;
            $output['message'] = 'Successfully updated '. $updated . tense( $updated, ' user' );   
        }
        else
        {
            $output['success'] = true;
            $output['message'] = 'Nothing to update';
        }
    }
    else
    {
        $output['success'] = false;
        $output['message'] = 'Nothing to save';
    }
    
    //Remove the removals bit if not needed
    if( empty( $output['removals'] ) )
        unset( $output['removals'] );
    

    return json_encode( $output );
}


/**
 * Function to allow admins to add new users
 */
function admin_add_user( $data )
{
    global $db;
    
    if( !check_admin() || !$db )
    {
        return '';
    }
    
    header( 'Content-type: application/json' );
    $output = array();
    
    if( isset( $data['new_user'] ) && $data['new_user'] == 'true' )
    {
        
        //Make sure we're working with valid data
        if( empty( $data['user_name'] ) || empty( $data['user_email'] ) || !user_valid_email( $data['user_email'] ) || !user_valid_id( $data['user_name'] ) )
        {
            return json_encode( array( 'success' => false, 'message' => 'Please enter a valid user name and email.' ) );
        }
        //Make sure the user doesn't already exist
        $number = user_exists( 'user_id', array( 'user_email' => "AES_ENCRYPT('".$db->filter( $data['user_email'] )."', '".E_ST."')" ) ); 
        if( $number )
        {
            return json_encode( array( 'success' => false, 'message' => $data['user_email'] . ' already exists' ) );
        }
        
        //Auto generate user password
        if( empty( $data['user_password'] ) )
        {
            $generated = user_generate_password();
            $password = user_system_password_hash( $generated );
        }
        //Use admin entered password
        else
        {
            $generated = $data['user_password'];
            $password = user_system_password_hash( $generated );
        }
        
        //Prep the array
        $details = array(
            'user_name' => "'".$db->filter( $data['user_name'] )."'", 
            'user_email' => "AES_ENCRYPT('".$db->filter( $data['user_email'] )."', '".E_ST."')", 
            'user_pwd' => "'".$db->filter( $password )."'", 
            'user_level' => "'".$db->filter( $data['user_level'] )."'", 
            'user_date' => "'".$db->filter( right_now() ) ."'", 
            'approved' => 1
        );
        
        //Insert safe since the data is pre-cleaned
        if( $db->insert_safe( USERS, $details ) )
        {
            $last_id = $db->lastid();
            $db->update( USERS, array( 'md5_id' => md5( $last_id ) ), array( 'user_id' => $last_id ) );
            $output['success'] = true;
        }
        
        //Send the user an email if notify is selected
        $m = false;
        if( isset( $data['notify_user'] ) && $data['notify_user'] == 1 )
        {
            $message = '<p>Hi '. stripslashes( $data['user_name'] ).'!</p>';
            $message .= '<p>A '.SITENAME.' administrator has created an account for you:</p>';
            $message .= '<p>Here are your login details ...<br />';
            $message .= 'User name: '. stripslashes( $data['user_name'] ) .'<br />';
            $message .= 'User Email: '.stripslashes( $data['user_email'] ).'<br />';
            $message .= 'Password: '.stripslashes( $generated ) .'</p>';
            $message .= '<p>You may log in at: <a href="'.BASE.'/users/">'.BASE .'/users/</a></p>';
            $message .= '<p>Thank You,<br />';
            $message .= 'Administrator<br />';
            $message .= SITENAME .'</p>';

            $subject = SITENAME.' Account Created';
            
            if( function_exists( 'send_message' ) )
            {
                $from = array( 'from' => USER_EMAIL_REPLYTO, 'name' => SITENAME );
                $m = send_message( $from, $data['user_email'], $subject, $message, '', 'You\'re being sent this message because a '. SITENAME. ' administrator created an account for you' );  
            }
            else
            {
                /**
                 * Fallback in the event that the plugin isn't initated
                 */
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: '.SITENAME.' System <'.$config['noreply_email'].'>' . "\r\n";
                $m = mail( $data['user_email'], $subject, $message, $headers );
            }
        } //end send message for new user
        
        $output['message'] = 'Successfully added '. $data['user_email'];
        if( $m )
        {
            $output['message'] .= ' and sent notification email';
        }
    
    } //end $data is correct
    else
    {
        $output['success'] = false;
        $output['message'] = 'Nothing to add';
    }
    
    return json_encode( $output );
}


/**
 * Securely log out users
 *
 * @access public
 * @param none
 * @return string header redirect
 *
 */
function user_system_logout( $redirect = '', $msg = '' )
{
    if( isset( $_SESSION['user_id'] ) )
    {
        global $db;
        if( !$db ) return;
                    
        $update = array(
            'ckey' => '',
            'ctime' => ''
        );
        $updated = $db->update( USERS, $update, array( 'user_id' => $db->filter( $_SESSION['user_id'] ) ) );

        unset( $_SESSION['user_id'] );
    }
    
    //Delete the sessions
    $_SESSION = array();
    session_unset();
    session_destroy();
    session_start();
    
    //Destroy cookies if applicable
    if( isset( $_COOKIE['user_id'] ) )
    {
        setcookie( "user_id", '', time()-60*60*24*COOKIE_TIME_OUT, "/" );
    }
    if( isset( $_COOKIE['user_key'] ) )
    {
        setcookie( "user_key", '', time()-60*60*24*COOKIE_TIME_OUT, "/" );
    }


    if( empty( $redirect ) || $redirect == BASE )
    {
        $redirect = str_replace( '?logout=true', '', BASE );
    }
    else
    {
        $redirect = BASE . '/login/?redirect='. urlencode( $redirect );
    }

    if( !empty( $msg ) )
    {
        $redirect .= '?msg='.urlencode( $msg );
    }
    
    //Generate a flash message to notify the user
    flash( 'user_logout', 'Logged out successfully!' );
    
    header( "Location: ".$redirect ."" );
    exit;
}

/* Secure pages */
function page_protect()
{
    global $db;
    if( !$db ) return;
    
    force_session();
    
    $cookie_check = validate_user_cookie();
    if( $cookie_check )
    {   
        $_SESSION['user_id'] = $_COOKIE['user_id'];
        /* query user level from database instead of storing in cookies */
        list( $user_level ) = $db->get_row( "SELECT user_level FROM ".USERS." WHERE user_id = '".$db->filter( $_SESSION['user_id'] )."'" );

        $_SESSION['user_level'] = $user_level;
        $_SESSION['HTTP_USER_AGENT'] = md5( $_SERVER['HTTP_USER_AGENT'] );
    }
    else
    {
        
        /**
         * Ensure all known session vars aside from user level have been set, if not, logout
         */
        if( !isset( $_SESSION['user_id'] ) || !isset( $_SESSION['HTTP_USER_AGENT'] ) || !isset( $_SESSION['fpckey'] ) || !isset( $_SESSION['fpstamp'] ) )
        {
            user_system_logout( $_SERVER['REQUEST_URI'] );
            exit;
        }

        /* Secure against Session Hijacking by checking user agent */
        if( isset( $_SESSION['HTTP_USER_AGENT'] ) )
        {
            if( $_SESSION['HTTP_USER_AGENT'] != md5( $_SERVER['HTTP_USER_AGENT'] ) )
            {
                user_system_logout();
                exit;
            }
        }

        /**
         * If we've gotten this far, we'll compare the session fpckey and fpstamp to the db values
         * If they don't match, cut the user out
         */

         $query = "SELECT `ckey`,`ctime` FROM `".USERS."` WHERE `user_id` = '".$db->filter( $_SESSION['user_id'] )."'";
         list( $ckey, $ctime ) = $db->get_row( $query );

         if( $ckey != $_SESSION['fpckey'] || $ctime != $_SESSION['fpstamp'] )
         {
            user_system_logout( $_SERVER['REQUEST_URI'] );
            exit;
         }
    }
}


/**
 * Function to output option for admins to switch between user levels
 * @access public
 * @param none
 * @return bool
 */
function admin_change_level()
{
    if( check_admin() || ( isset( $_SESSION['fp_st_ladj'][$_SESSION['user_id']] ) && !empty( $_SESSION['fp_st_ladj'][$_SESSION['user_id']] ) ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}


/**
 * Allow admins to view site as researcher or participant
 * @access public
 * @param int level
 * @return none
 */
function adjust_level( $level = 3 )
{
    if( admin_change_level() && $level < ADMIN_LEVEL )
    {
        //If the user is authorized to do this, allow it and override the session var
        if( empty( $_SESSION['fp_st_ladj'][$_SESSION['user_id']] ) || !isset( $_SESSION['fp_st_ladj'][$_SESSION['user_id']] ) )
        {
            $_SESSION['fp_st_ladj'][$_SESSION['user_id']] = true;
        }    
        unset( $_SESSION['user_level'] );
        $_SESSION['user_level'] = $level;
    }
    elseif( admin_change_level() && $level == ADMIN_LEVEL )
    {
        //if admin switches back to admin, just clear the variable
        unset( $_SESSION['user_level'] );
        unset( $_SESSION['fp_st_ladj'] );
        $_SESSION['user_level'] = $level;
    }
    elseif( !admin_change_level() && ( empty( $_SESSION['fp_st_ladj'][$_SESSION['user_id']] ) || !isset( $_SESSION['fp_st_ladj'][$_SESSION['user_id']] ) ) )
    {
        //If we can't realistically authenticate the user as being able to do this, really don't allow
        unset( $_SESSION['fp_st_ladj'] );
        user_system_logout();
        exit;
    }
    else
    {
        unset( $_SESSION['fp_st_ladj'] );
        user_system_logout();
        exit;
    }
}


/**
 * Check for $_GET IF applicable to change the level
 */
if( !empty( $_GET['change_level'] ) && admin_change_level() )
{
    adjust_level( $_GET['change_level'] );
    flash( 'level-change', 'You are now a level '. $_GET['change_level'].' user' );
    header( "Location: ".USER_DEFAULT_MAIN );
    exit;
}

/**
 * Check to see if a user is logged in at all
 * @access public
 * @param none
 * @return bool
 */
function logged_in()
{
    if( isset( $_SESSION['user_level'] ) && isset( $_SESSION['user_id'] ) && isset( $_SESSION['fpckey'] ) && isset( $_SESSION['fpstamp'] ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}



/**
 * Check for admin
 * @access public
 * @param none
 * @return bool
 */
function check_admin()
{ 
    if( isset( $_SESSION['user_level'] ) && $_SESSION['user_level'] == ADMIN_LEVEL )
    {
        //Add the users styles to the header
        add_output( 'header', 'user_system_admin_page_css' );
        //Add the users.js file to the footer
        add_output( 'footer', 'user_system_admin_page_js' );
        return true; 
    }
    else
    {
        return false; 
    } 
}


/**
 * Check for editors
 * @access public
 * @param none
 * @return bool
 */
function check_editor()
{
    if( isset( $_SESSION['user_level'] ) && $_SESSION['user_level'] >= EDITOR_LEVEL )
    { 
        return true; 
    }
    else
    {
        return false;
    }
}


/**
 * Check for normal users
 * @access public
 * @param none
 * @return bool
 */
function check_user()
{      
    if( isset( $_SESSION['user_level'] ) && $_SESSION['user_level'] >= USER_LEVEL )
    {
        //Add the users styles to the header
        add_output( 'header', 'user_system_page_css' );
        //Add the users.js file to the footer
        add_output( 'footer', 'user_system_page_js' );
        return true; 
    }
    else
    {
        return false;
    }   
}


/**
 * Function to redirect non admin users back to normal user only areas
 * @access public
 * @param none
 * @return mixed
 */
function force_admin()
{
    if( !check_admin() )
    {
        header( 'Location: ' . USER_DEFAULT_MAIN );
        exit;
    }
}



/**
 * Include facebook system if FB_ACTIVE is true in user-config.php
 */
if( FB_ACTIVE )
{
    require( 'facebook/facebook-config.php' );
}

/* End of file user-functions.php */
/* Location: application/plugins/user-system/user-functions.php */