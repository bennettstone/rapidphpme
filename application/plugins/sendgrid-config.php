<?php
/**
 * sendgrid-config.php
 * Requires sendgrid account
 * http://sendgrid.com
 *
 * @version 1.0
 * @date 07-Feb-2013
 * @package Sendgrid PHP Master
 **/


//Define sendgrid credentials
define( 'SENDGRID_USER', 'yourusername' );
define( 'SENDGRID_PASS', 'yourpassword' );


/**
 * Updated function to deliver batch and single emails via sendgrid
 * THIS FUNCTION SUPERSEDES BOTH THE send_message AND send_sendgrid FUNCTIONS
 * Uses https://github.com/sendgrid/sendgrid-php classes
 * 
 * Usage:
 * $send_to = array( 'you@gmail.com' => 'Your Name', 'anotherperson@email.com' => 'Another Name' );
 * $from = array( 'from_name' => SITENAME, 'from_email' => 'no-reply@'.strtolower( SITENAME ).'.com', 'reply_to' => 'optional@gmail.com' );
 * $subject = 'This is amazing!';
 * $html = '<html><body><h1>Hey -username-!</h1><p>Test messages are great!</p></body></html>';
 * $category = 'administrative';
 * $subs = array( '-username-', array( 'YOU!', 'Another!' ) );
 *
 * send_sendgrid( $send_to, $from, $subject, $html, $category, $subs );
 * 
 * @access public
 * @param array key => value array of name => email addresses
 * @param array from and reply-to
 * @param string subject
 * @param string html content
 * @param string category (optional, defaults to participation)
 * @param array substitution values array( '-replace_this_string-', array( 'with me', 'and me' ) )
 * @return bool success or failure
 */
function send_via_sendgrid( $send_to = array(), $from = array(), $subject, $html, $category = 'participation', $subs = array() )
{
    
    $sendgrid = new SendGrid( SENDGRID_USER, SENDGRID_PASS );
    
    //Takes array( 'from_name' => 'Name',  'from_email' => 'you@email.com', 'reply_to' => 'replytome@email.com' ) format
    extract( $from );
    $from_name = isset( $from_name ) ? $from_name : '';
    $from_email = $from_email;

    $mail = new SendGrid\Mail();
    $mail->setFrom( $from_email );
    $mail->setFromName( $from_name );
    
    if( isset( $reply_to ) )
        $mail->setReplyTo( $reply_to );
    
    /**
     * Use full array( 'email@email.com' => 'your name' ) format
     */
    foreach( $send_to as $email => $name )
    {
        $mail->addTo( $email, $name );
    }
    $mail->setSubject( $subject );
    
    $plaintext = str_replace( array( '</p>', '<br />', '<br>' ), PHP_EOL, $html );
    $plaintext = strip_tags( $plaintext );
    $mail->setText( $plaintext );   
    
    //Autowrap the message in the FP wrapper
    $outbound_message = '';
    $outbound_message .= $html;
    
    $mail->setHtml( $outbound_message );
    
    /**
     * Process any substitutions
     * Should be passed as associative array
     * array( '-replaceme-', array( 'with me', 'and me', 'and me' ) )
     */
    if( !empty( $subs ) )
    {
        foreach( $subs as $attr => $val )
        {
            $mail->addSubstitution( $attr, $val );
        } 
    }
    $mail->addCategory( $category );

    return $sendgrid->smtp->send( $mail );   
}