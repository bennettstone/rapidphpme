<?php
/**
 * fastcache-config.php
 * Loads up phpfastcache, settings for caching names
 *
 * @version 1.0
 * @date 19-Jan-2014
 * @package RapidPHPMe
 **/
if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );

$cache_config = Options::get_cache();

//Define a default cache length of 1 hour in seconds
define( 'CACHE_EXPIRE_DEFAULT', $cache_config['expiry'] );

//Define db table name to store cache creation logs in
//Only enable this if you want to provide an admin interface to view/clear cachedata
define( 'CACHE_DB', PREFIX . 'cachenames' );

//Define filecache location
define( 'CACHE_FILE_LOC', $cache_config['path'] );

/**
 * Have the cache file make it's own directory and include a silencer file before we get too far
 */
if( !is_dir( $cache_config['path'] ) )
{
    mkdir( $cache_config['path'] );
    //Add a noindex file
    $handle = fopen( $cache_config['path'] . SEP . 'index.php', 'a' ) or trigger_error( 'Cannot open file:  '.$cache_config['path'] . SEP .'index.php' );
    $contents = '<?php' . PHP_EOL;
    $contents .= '//Silence!'. PHP_EOL;
    fwrite( $handle, $contents );
    fclose( $handle );
}

$cache_config = array(
    'storage' => $cache_config['driver'], 
    'path' => $cache_config['path'], 
    'fallback' => array(
        'example' => 'files', 
        'memcache' => 'files'
    ), 
    'securityKey' => '', 
    'htaccess' => true, 
    'default_chmod' => 0777, 
    'server' => $cache_config['memcached'],
);
phpFastCache::setup( $cache_config );
global $cache;
$cache = phpFastCache();


/**
 * Function to clear the fastcache when content gets updated by admins
 * Uses switch params based on type
 *
 * @access public
 * @param array $removal_names
 * @return bool
 */
function clear_cache( $removal_names = array() )
{
    global $cache;
    
    //Only attempt to clear the cache if we have a cachename to remove
    if( !empty( $removal_names ) )
    {
        foreach( $removal_names as $r )
        {
            $cache->delete( $r );
        }
        return true;
    }
    else
    {
        return false;
    }
}


//List of cached items, mainly to keep track of anything that could become problematic
$cached_items = array();