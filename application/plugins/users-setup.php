<?php
/**
 * users-setup.php
 * Assigns variables used for the userClass plugin
 * Assigns default redirects
 * Assigns system language
 *
 * @version 1.0
 * @date 29-Nov-2014
 * @package userClass
 **/
if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );

//Define the users table name if not already defined
if( !defined( 'USERS' ) )
{
    $prefix = Options::get_database();
    $prefix = $prefix['prefix'];
    define( 'USERS', $prefix . 'users' );
}

global $userclass, $db;
$options = array(
    //require email conf. for normal reg?
    'confirm_users' => false,

    //require email conf. for social media auth?
    'confirm_social_users' => false, 
    
    //send users a reg success email?
    'send_reg_success' => true, 

    //database table name to use for users
    'users_table' => USERS,  

    //Force header() location redirect?           
    'header_redirect' => false,

    //Reg with email OR username/email
    'email_only' => true,
    
    //If Crypt functions are availble, use them?
    'use_crypt' => true,

    //Trailing slash IS important!
    'url' => BASE . SEP,

    //Multiplier for password hashing
    'hash_cost_factor' => 10,

    //2 weeks for secure cookie
    'cookie_runtime' => 1209600, 

    // important: always put a dot in front of the domain, like ".mydomain.com" see http://stackoverflow.com/q/9618217/1114320
    'cookie_domain' => '.' . $_SERVER['HTTP_HOST'], 

    //Name of cookie to set
    'cookie_name' => 'remember_me', 

    //Outbound email "from" email
    'from_email' => 'no-reply@'. $_SERVER['HTTP_HOST'] .'.com', 

    //Outbound email "from" name
    'from_name' => SITENAME, 

    //Full URI to where you plan on having the activate_account function
    'email_verification_url' => '', 

    //Subject line for account activation emails
    'email_verification_subject' => 'Account activation for '. SITENAME, 
    
    'reg_success_subject' => SITENAME . ' Account created successfully', 

    //Password reset URI where verify_pass_reset() will be
    'email_password_reset_url' => '', 

    //Password reset email subject
    'email_password_reset_subject' => 'Password reset for '. SITENAME, 

    //Logout redirect for page_protect() (aka, login page)
    'secure_redirect' => 'users/login', 

    //Oauth credentials
    'oauth' => array(
        'fb' => array(
            'client_id' => '', 
            'client_secret' => '', 
            'client_redirect' => 'users/authorize/facebook/'
        ), 
        'google' => array(
            'client_id' => '', 
            'client_secret' => '', 
            'client_redirect' => 'users/authorize/google/'
        )
    ), 
    
    //User levels
    'user_levels' => array(
        'user' => 1, 
        'admin' => 5
    )
);

$userclass = new userClass( $db, $options );