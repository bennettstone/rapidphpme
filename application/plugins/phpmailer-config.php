<?php
/**
 * phpmailer-config.php
 * Provides assistive function to use PHPMailer class
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 07-Feb-2013
 * @package PHP Mailer Plugin
 **/

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );


/**
 * Send messages using phpmailer
 * For SMTP, define user, pass, location, and port in global index.php or config.php
 *
 * @param $from
 * @param $to
 * @param $subject
 * @param $message_content
 * @param string $reply_to
 * @param array $replacements
 * @return bool
 */
function send_message( $from, $to, $subject, $message_content, $reply_to = '', $replacements = array() )
{

    //Initiate the mailer class
    $mail = Mailer::init();

    //Set the sender and receiver email addresses
    //Can use key value array as param array( 'from' => 'user@site.com', 'name' => 'Site admin' )
    if( is_array( $from ) )
    {
        extract( $from );
        $mail->SetFrom( $from, $name, 0 ); 
    }
    else
    {
        $mail->SetFrom( $from, "" );   
    }
    
    //Allow more specific details associated with sender reply-to
    if( !empty( $reply_to ) && is_array( $reply_to ) )
    {
        extract( $reply_to );
        $mail->AddReplyTo( $reply_email, $reply_name );   
    }
    
    if( is_array( $to ) )
    {
        foreach( $to as $i )
        {
            $mail->AddAddress( $i, "" );
        }
    }
    else
    {
        $mail->AddAddress( $to, "" );   
    }

    //Set the message subject
    $mail->Subject = $subject;

    $data = [];
    if( !empty( $replacements ) )
    {
        $data = array_merge( $data, $replacements );
    }
    $message = Mailer::parse_template( $data, $message_content );

    //Send the message as HTML
    $mail->MsgHTML( stripslashes( $message ) ); 

    //Display success or error messages
    if( !Mailer::SendOut( $mail ) )
    {
        trigger_error( 'Message send failure: ' . $mail->ErrorInfo );
        return false;
    }
    else
    {
        return true;
    }

}

/* End of file phpmailer-config.php */
/* Location: application/plugins/phpmailer/phpmailer-config.php */