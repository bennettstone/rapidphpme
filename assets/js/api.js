function log(data, color) {
	var color = (typeof(color) !== 'undefined') ? color : 'green';
	if(typeof(site_options.debug) !== 'undefined' && site_options.debug) {
		if(typeof(data) === 'object') {
			data = JSON.stringify(data);
		}
		console.log("%c%s", "color: white; background: " + color + "; font-size: 18px;", data);
	}
}

//Google analytics event tracking
function track_action(category, event, string, value) {
	if(typeof ga == 'undefined'){
		log('ga is undefined: ' + category + ' ' + event + ' ' + string);
	} else {
		ga('send', 'event', category, event, string, value);  // value is a number.
	}
}


/**
 * Ajax post API function
 *
 * Takes set of params associated with prespecified inputs
 * parameters = new Object();
 * parameters.uri = 'http://something.com/image.jpg';
 * parameters.label = 'Awesome pic';
 * parameters.created = '2012-01-01';
 * api('api/process/', parameters, function(data){
 *      console.log(data);
 * });
 */
function api(endpoint, parameters, callback){

	if(typeof(site_options) === 'undefined') {
		log('No site options defined.');
	}
	var api_parameters = 'randkey='+new Date().getTime();

	if(parameters instanceof Array){
		$.each(parameters, function(key, value) {
			parameters += '&'+key+'='+value;
		});
	}

	ajax_url = site_options.base + '/'+endpoint+'/';

	$.ajax
	({
		type: "POST",
		url: ajax_url,
		data: parameters,
		success: callback,
		error: function(response, textStatus, errorThrown){
			if(typeof(response.responseJSON) !== 'object') {
				console.log(errorThrown + ' trying to retrieve data from ' + ajax_url);
			} else {
				response = response.responseJSON;
				response.error.request = ajax_url;
				log(response.error, 'red');
			}
		}
	});
}


//Check if element is defined
function exists(element) {
    if(jQuery(element) !== undefined && jQuery(element).length > 0) {
        return true;
    }
    return false;
}

/**
 *
 * @param count
 * @param toTense
 * @param append
 * @returns {*}
 */
function tense(count, toTense, append ) {
	var str,
		append = (typeof(append) !== 'undefined') ? append : 's',
		count = parseInt(count);
	if( count == 0 || count >= 2 ) {
		str = toTense + append;
	} else {
		str = toTense;
	}
	return str;
}


/**
 * Open a new window
 * @param link
 */
function window_sharer(link) {
	window.open(link, 'sharer', 'toolbar=0,status=0,width=548,height=325');
}