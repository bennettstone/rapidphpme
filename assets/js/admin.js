function hit_uploader() {
    var uploader = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',
        browse_button : 'pickfiles', // you can pass in id...
        container: document.getElementById('pupload_container'), // ... or DOM Element itself
        url : site_options.base + '/api/chunk-upload/' + jQuery('#pickfiles').data('path'),
        flash_swf_url : site_options.base + '/assets/js/upload/Moxie.swf',
        silverlight_xap_url : site_options.base + '/assets/js/upload/Moxie.xap',

        filters : {
            max_file_size : '4mb', 
            chunk_size: '1mb', 
            resize: false,
            mime_types: [
                {title : "Spreadsheet files", extensions : "csv,xls,xlsx"}
            ]
        },
        init: {
            PostInit: function() {
                jQuery('#filelist').empty();
            },
            FilesAdded: function(up, files) {
                plupload.each(files, function(file) {
                    jQuery('#filelist').append('<div id="uploaded_' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>');
                });
                uploader.start();
                jQuery('.js-in-modal').attr('disabled', 'disabled');
            },
            UploadProgress: function(up, file) {
                jQuery('#pupload_bar').show();
                jQuery('#pupload_bar .bar').css('width', file.percent + '%');
            },
            Error: function(up, err) {
                jQuery('#console').html("\nError #" + err.code + ": " + err.message);
            },
            FileUploaded: function(up, file, info) {
                console.log(info);
                var data = JSON.parse(info.response);
                if(data.success == true) {
                    jQuery('#uploaded_' + file.id).remove();
                    jQuery('#image_addlist').append('<li><img style="width: 100%" src="' + data.filename + '" class="th" /><input type="hidden" id="uploaded_' + data.id + '" name="image[]" value="' + data.base_filename + '" /></li>');
                }
                jQuery('#pupload_bar .bar').css('width', '0%');
                jQuery('#pupload_bar').fadeOut();
                jQuery('.js-in-modal').removeAttr('disabled');
            }
        }
    });
    uploader.init();
}

jQuery(document).ready(function($){
    
    $(document.body).on('click', '.clear-uploaded', function(e){
        e.preventDefault();
        var thisParent = $(this).closest('.image-upload-grid');
        var removeFile = thisParent.find('img').attr('src');
        params = new Object();
        params.remove = true;
        params.filename = removeFile;
        if($(this).data('id') !== undefined){
            params.file_id = $(this).data('id');
        }
        api('api/chunk-upload', params, function(data){
            if(data.success == true)
            {
                thisParent.remove();
            }
        });
    });
    
    //Enable jwysiwyg texteditor
    if($('textarea.editor-class').length) {
        $('textarea.editor-class').wysiwyg({
            //css: siteBase + "/assets/css/all-css.min.css", 
            autoGrow: true, 
            maxHeight: 600, 
            autoSave: true, 
            rmUnusedControls: false, 
            controls: {
                bold: { visible: true }, 
                strikeThrough: { visible: true }, 
                italic: { visible: true },
                underline: { visible: true },
                subscript: { visible: true },
                superscript: { visible: true }, 
                insertOrderedList: { visible : true }, 
                insertUnorderedList: { visible: true }, 
                insertHorizontalRule: { visible: true }, 
                createLink: { visible: true }, 
                unLink: { visible: true }, 
                h1: { visible: true },
                h2: { visible: true }, 
                h3: { visible: true }, 
                html: { visible: true }
            }
        });
        $.wysiwyg.fileManager.setAjaxHandler(siteBase + "/api/admin-upload");   
    }

    //Datepicker
    $('input.datepicker').Zebra_DatePicker({ format: 'Y-m-d' });

    //Display normal forms removing disabled class as neede
    $('body').on('click, focus', 'form#admin_users_form input.clearable', function(){
        if($(this).hasClass('disabled')) {
            $(this).removeClass('disabled');
        }
        return false;
    });
    //Display normal forms removing disabled class as neede
    $('body').on('blur', 'form#admin_users_form input.clearable', function(){
        if(!$(this).hasClass('disabled')) {
            $(this).addClass('disabled');
        }
        return false;
    });
    //Add a modified flag to rows so not everything needs to be saved every time
    $('body').on('change', 'form#admin_users_form input, form#admin_users_form select', function(){
        $(this).closest('tr').find('.admin_altered_row').val('true');
    });

    
    //Clone week/day fields for /admin/recipe/add
    $(document.body).on('click', '.duplicate_days', function(e){
        e.preventDefault();
        var putInto = $(this).closest('.parent_clone').data('duplicate');
        $(putInto).append($(this).closest('.parent_clone').clone());
    });
    
    /*******************************
    Start admin/recipes/
    ********************************/
    //Auto update recipe title slugs
    $(document.body).on('keyup', 'input.recipe-title-area', function(e){
        var textVal = $(this).val();
        textVal = textVal.replace(/\s+/g, '-').toLowerCase();
        textVal = textVal.replace(/[^0-9a-z-\-]/g,"");
        $('span.recipe-title-slug').text(textVal + '/');
        $('input[name="recipe_slug"]').val(textVal);
    });

    //Prevent anything except numbers from being entered in the quantity for ingredients
    $(document.body).on('keypress', 'input.quantity-only', function(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        return !(charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57));
    });

});

/* End of file admin.js */
/* Location: application/plugins/user-system/js/admin.js */