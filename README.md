RapidPHPMe
========

To get started:

* open terminal (applications -> utilities -> terminal)

* type "cd" and drag this folder into the terminal window, then hit enter to make this the current working directory

* enter: "php composer.phar install"

* done.

##Step 2

* Put your controllers in the /application/controllers/[anything]/ directories

* Put your views in the /application/views/[anything]/ directories

* Set your configuration files in the /application/config/ folders

* Put your models in the /application/models/ directory

* Put any necessary .env.[environment] files into the root directory, these are ignored by default using the .gitignore for security

* For automatic deployment, after the initial composer.phar install, you'll see a file named "deploy.ini" in the project root.  You can edit this to deploy to servers from your local.  See https://github.com/banago/PHPloy for more info

* If using Rocketeer, deploy by typing: php vendor/bin/rocketeer deploy --stage="production[/staging]"

##Before uploading to production...

* In terminal, run: "composer dump-autoload --optimize" to reduce memory usage during lookups

##Command line helpers...

* After the initial composer install has been run, you can auto-generate new models, controllers, and view templates by running... "composer new-model" which will prompt you to enter a list of Classes and their respective inner functions (if you need them)
** For example, "Admin : login | manage_users, Users : settings | logout" would generate two new class files in "application/controllers/"; "Admin.php" which would contain "public function login()", "public function manage_users", and "Users.php" which would contain "public function settings()", and "public function logout()", as well as corresponding views in the "application/views/" directory called: "manage-users.php", "login.php", "settings.php", and "logout.php"

##Troubleshooting deployments using SSH

When using rocketeer, it's probable that at some point you will receive "Permission Denied (Publickey)" errors and your deployments will fail.  If this happens, it's likely that the SSH key on the server has not been set as a deployment key, and may need to be added both to gihub/bitbucket, and/or the server itself.

###To add to the server

* SSH into your server as the same user you've coded as the SSH user
* cd ~/.ssh
* ssh-keygen -t rsa -b 2048 -f ~/.ssh/id_rsa -C "Comment or title for the key"
* You probably will NOT want to add a passphrase for the key as your deployments will silently fail
* chmod 600 ~/.ssh/authorized_keys && chmod 700 ~/.ssh/
* cat ~/.ssh/id_rsa.pub
* Copy-paste the cat'd output into the admin/deploy-keys/ section of your repository in bitbucket or github
* Go back to the SSH terminal and run... ssh -Tv git@bitbucket.org OR ssh -vT git@github.com.  If all is well, you'll see a success message, otherwise, you may need to double check the deploy keys and/or ssh key within the server

See: 

* https://help.github.com/articles/error-permission-denied-publickey/
* https://mediatemple.net/community/products/dv/204644740/using-ssh-keys-on-your-server
* https://confluence.atlassian.com/bitbucket/troubleshoot-ssh-issues-271943403.html