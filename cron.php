<?php
/**
 * cron.php
 *
 * Used to bypass weird SSL cert errors from cloudflare or just to load up pages
 * In crontab:
 * Run a php script with the path to this file with space separated uris as arguments
 * I.e...
 * 3 * * * /path_to_script/cron.php http://test.com http://test-two.com
 *
 * @version 1.0
 * @date 8/5/16 2:21 PM
 * @package rapidphpme
 */

if( isset( $argv ) && count( $argv ) > 1 )
{
	unset( $argv[0] );
	$uris = array_map( 'trim', $argv );
	if( empty( $uris ) )
	{
		echo 'No URIs to load';
		die;
	}

	$responses = array();
	foreach( $uris as $link )
	{
		set_time_limit(400);

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $link );
		curl_setopt( $ch, CURLOPT_HEADER, false );
		curl_setopt( $ch, CURLOPT_NOBODY, true );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 100 );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );

		$response = curl_exec( $ch );

		$curl_errno = curl_errno( $ch );
		$curl_error = curl_error( $ch );
		$response_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

		curl_close( $ch );

		if( $curl_errno > 0 )
		{
			$responses[] = "cURL Error ({$curl_errno}): {$curl_error} when trying to retrieve {$link}";
		}

		if( $response_code > 200 )
		{
			$responses[] = $response_code .' for '. $link;
		}
		else
		{
			$responses[] = $link . ' processed successfully';
		}

	}
	if( !empty( $responses ) )
	{
		echo '<pre>';
		print_r( $responses );
		echo '</pre>';
	}
}