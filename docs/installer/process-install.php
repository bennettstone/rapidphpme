<?php
/**
 * process-install.php
 * Provides easier installation and config for Tom!
 *
 *
 * @version 2.1
 * @date 18-Jul-2013
 * @updated 30-Jun-2014
 * @package RapidPHPMe
 **/
if( isset( $_POST ) && !empty( $_POST ) )
{
    $files = array(
        'environments' => array(
            'development' => '../application/config/environments/development.php', 
            'staging' => '../application/config/environments/staging.php', 
            'production' => '../application/config/environments/production.php', 
        ), 
        'autoload' => '../application/config/autoload.php', 
        'config' => '../application/config/config.php', 
    );
    
    $save_status = array();
    
    if( !empty( $_POST['index'] ) && is_array( $_POST['index'] ) )
    {
        foreach( $_POST['index'] as $environment => $params )
        {
            $filename = $files['environments'][$environment];
            if( !file_exists( $filename ) )
            {
                continue;
            }
            
            $file = file( $filename );
            $file_contents = array();
            foreach( $file as $line )
            {
                foreach( $params as $key => $value )
                {
                    if( $value == 'true' || $value == 'false' )
                    {
                        $value = $value;
                    }
                    else
                    {
                        $value = "'$value'";
                    }
                    $key = strtoupper( $key );
                    $find["define( '$key'"] = "define( '$key', $value );";
                    
                    if( strpos( $line, "define( '$key'" ) !== false )
                    {
                        $line = "define( '$key', $value );" . PHP_EOL;
                    }

                }
                $file_contents[] = $line;
            }
            $clean_contents = implode( '', $file_contents );
            if( file_put_contents( $filename, $clean_contents ) )
            {
                $save_status[] = $filename .' updated';   
            }
        }
    } //end environments
    
    //Autoload file
    if( isset( $_POST['autoload'] ) && !empty( $_POST['autoload'] ) && is_array( $_POST['autoload'] ) )
    {
        $filename = $files['autoload'];
        if( file_exists( $filename ) )
        {
            $file = file( $filename );
            $add = array();
            $file_contents = array();
            $before = 0;
            foreach( $file as $l => $line )
            {
                foreach( $_POST['autoload'] as $auto )
                {
                    $autoload_string = "\$autoload[] = '".$auto."';";
                    //If it's already autoloaded, ignore it
                    if( strpos( $line, $autoload_string ) !== false )
                    {
                        continue;
                    }
                    else
                    {
                        $add[$auto] = $autoload_string . PHP_EOL;
                    }
                    
                    //Find the end of the file
                    if( strpos( $line, 'End of file' ) )
                    {
                        $before = $l-1;
                    }
                }
                
                $file_contents[] = $line;
            }
            if( !empty( $add ) )
            {
                $string = '';
                foreach( $file_contents as $l => $new )
                {
                    if( isset( $before ) && $before > 0 && $l == $before )
                    {
                        $string .= implode( '', $add ) . PHP_EOL;
                        unset( $add );
                    }
                    else
                    {
                        $string .= $new;   
                    }
                }
                if( file_put_contents( $filename, $string ) )
                {
                    $save_status[] = $filename .' updated';   
                }
            }
        }
        else
        {
            $save_status[] = $filename .' does not exist.  Can\'t update';
        }
    }
    //End autoload file
    
    //Config
    if( isset( $_POST['config'] ) && is_array( $_POST['config'] ) && !empty( $_POST['config'] ) )
    {
        $filename = $files['config'];
        if( file_exists( $filename ) )
        {
            $base_params = array();
            if( isset( $_POST['config']['sitename'] ) && !empty( $_POST['config']['sitename'] ) )
            {
                $base_params['SITENAME'] = $_POST['config']['sitename'];
            }
            if( isset( $_POST['config']['prefix'] ) && !empty( $_POST['config']['prefix'] ) )
            {
                $base_params['PREFIX'] = $_POST['config']['prefix'];
            }
            if( isset( $_POST['config']['e_st'] ) && !empty( $_POST['config']['e_st'] ) )
            {
                $base_params['E_ST'] = $_POST['config']['e_st'];
            }
            
            $file = file( $filename );
            $file_contents = array();
            $endfile = 0;
            foreach( $file as $l => $line )
            {
                //Loop through and deal with the defined vals
                foreach( $base_params as $k => $v )
                {
                    if( strpos( $line, "define( '$k'" ) !== false )
                    {
                        $line = "define( '$k', '$v' );" . PHP_EOL;
                    }
                }

                //Find the end of the file
                if( strpos( $line, 'DB Creds go here' ) )
                {
                    $endfile = $l+1;
                }

                $file_contents[] = $line;
            }
            
            //DB connection details
            $add = array();
            foreach( $_POST['config']['db'] as $type => $params )
            {
                if( is_array( $params ) )
                {
                    foreach( $params as $key => $value )
                    {
                        $add[] = "\$connection['$type']['$key'] = '".$value."';" . PHP_EOL;   
                    }   
                }
            }
            
            //And put all the contents back into the file!
            $string = '';
            foreach( $file_contents as $l => $new )
            {
                if( !empty( $add ) && isset( $endfile ) && $endfile > 0 && $l == $endfile )
                {
                    $string .= implode( '', $add ) . PHP_EOL;
                    unset( $add );
                }
                else
                {
                    $string .= $new;   
                }
            }
            if( file_put_contents( $filename, $string ) )
            {
                $save_status[] = $filename .' updated';   
            }

        }
        else
        {
            $save_status[] = $filename .' does not exist.  Can\'t update';
        }
    }
}