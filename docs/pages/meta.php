<?php
/**
 * meta.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 18-Jul-2014
 * @package RapidPHPDocs
 **/
?>
<div class="row">
    <div class="medium-12 columns">
        <h1>Meta Tag Generation</h1>
    </div>
</div>

<div class="row">
    <div class="medium-12 columns">
            <p>Through the "metaHelper" class (/application/core/class.meta.php), you can easily add commonly used (and generally annoying to remember) meta tags to the head of your website, as well as stylesheets and javascript files.</p>

            <p>Initiating and using the class is a snappity snap snap, and it's automatically loaded as part of the core package, so no need to include it separately.</p>

        <pre><code class="language-php">//Initiate the class
$meta_tags = new metaHelper();

//Include a couple stylesheets
$scripts = array(
    ASSETS .'/css/base.css', 
    ASSETS .'/css/skeleton.css', 
    ASSETS .'/css/layout.css', 
    ASSETS .'/css/custom.css'
);
$meta_tags-&gt;load_script( $scripts );

//Output the html5shim
$meta_tags-&gt;meta( 'html5', 'true' );</code></pre>

            <h2>Currently supported tags include:</h2>
            <ul class="featurelist">
                <li>charset: $meta_tags-&gt;meta( 'charset', 'UTF-8' );</li>
                <li>keywords: $meta_tags-&gt;meta( 'keywords', 'this, site, is, so, awesome' );</li>
                <li>description: $meta_tags-&gt;meta( 'description', $desc );</li>
                <li>author: $meta_tags-&gt;meta( 'author', 'Bennett Stone' );</li>
                <li>favicon:  $meta_tags-&gt;meta( 'favicon', ASSETS .'/images/favicon.ico' );</li>
                <li>og:title: $meta_tags-&gt;meta( 'og:title', 'The RapidPHPMe Framework by Bennett Stone' );</li>
                <li>og:type: $meta_tags-&gt;meta( 'og:type', 'website' );</li>
                <li>fb:app_id: $meta_tags-&gt;meta( 'fb:app_id', 'yourfacebookappid' );</li>
                <li>html5 shim: $meta_tags-&gt;meta( 'html5', 'true' );</li>
                <li>And custom meta tags by using the "$meta_tags-&gt;meta( 'name', 'content' );" parameters</li>
            </ul>

<h2>Including Javascript and CSS Files</h2>

    <p>The metaHelper class can handle arrays of both js and css file types, and will <strong>automatically group and organize them for</strong> you based on type.  For example, the following code shows input and output variances:</p>

        <pre><code class="language-php">//Initiate the metaHelper class in header.php
$header = new metaHelper();

//Add some css and js files (randomly)
$scripts_and_styles = array(
    'jquery', 
    ASSETS .'/js/jquery-clipboard.js', 
    ASSETS .'/css/custom.css', 
    ASSETS .'/js/script.js', 
    ASSETS .'/css/base.css'
);
$header-&gt;load_script( $scripts_and_styles, false, true );

//Which will output...
&lt;link rel="stylesheet" href="http://yoursite.com/assets/css/custom.css" /&gt;
&lt;link rel="stylesheet" href="http://yoursite.com/assets/css/base.css" /&gt;
&lt;script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"&gt;
&lt;script src="http://yoursite.com/assets/js/jquery-clipboard.js"&gt;
&lt;script src="http://yoursite.com/assets/js/script.js"&gt;
&lt;script&gt;if(typeof jQuery=='undefined'){document.write('&lt;script type="text/javascript" src="http://yoursite.com/assets/js/jquery-1.8.2.min.js" charset="utf-8"&gt;&lt;'+'/script&gt;');}</code></pre>
    </div>
</div>