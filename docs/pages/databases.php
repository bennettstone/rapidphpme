<?php
/**
 * databases.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 18-Jul-2014
 * @package RapidPHPDocs
 **/
?>
<div class="row">
    <div class="medium-12 columns">
        <h1>Working with Databases</h1>
        <p>RapidPHP comes bundled with SimpleMySQLi and is designed to use the version available on <a href="https://github.com/bennettstone/simple-mysqli" target="_blank">GitHub here</a>.</p>
    </div>
</div>

<div class="row">
    <div class="medium-12 columns">

        <h2>Initiating the database class</h2>
        <p>So simple.  Open the file: /application/config/autoload.php and make sure the following line exists:</p>
        <pre><code class="language-php">$autoload[] = 'database';</code></pre>
        <p>After which, to access the database functions from <strong>within a function or controller</strong>, simply the following before your queries:</p>
        <pre><code class="language-php">global $db;</code></pre>
        <p>For example, let's say THIS file was access via controller_databases.php, the code would look something similar to:</p>
        <pre><code class="language-php">&lt;?php
function controller_databases()
{
    global $db;
    
    //Get the page title
    list( $title, $page_name ) = $db->get_row( "SELECT title, name FROM pages WHERE slug = 'databases' LIMIT 1" );
    
    //Assign the title variable to be available as $title in header.php
    view( 'header', array( 'title' => $title ) );
    
    //Assign the heading for this page available in the view as $page_heading
    view( 'databases', array( 'page_heading' => $page_name ) );
    
    view( 'footer' );
}</code></pre>
        <p>The $db global is already available from within any file loaded via the "view()" function, so there is no need to call the "global $db;" param within views.</p>
        
        <p><em>Note: <strong>Unless otherwise indicated, all examples below assume the $db variable is already available</strong></em></p>

        <h2>Sanitizing Data</h2>
        <p><strong>It is important to sanitize any data that interacts with a database!</strong>  This class does not auto filter data for you!  But fortunately, you can autofilter any array or string values easily...</p>
        <pre><code class="language-php">//Sanitize ALL post values
$_POST = $db->filter( $_POST );

//Sanitize all get values
$_GET = $db->filter( $_GET );

//Sanitize a form value
$name = $db->filter( $_POST['name'] );

//Or a search string via get
$search = $db->filter( $_GET['s'] );
</code></pre>

        <h2>Retreiving query results as an object OR an array</h2>
        <p>Seems trivial, but the usage is very different.  Objects use the $variable->key format, while the $db class defaults to array.</p>
        <p>To retrieve query values as an object, simply pass a second parameter to any of the following functions:</p>
        <ul>
            <li>$db-&gt;get_row( "your query", true );</li>
            <li>$db-&gt;get_results( "your query", true );</li>
        </ul>
        
        <pre><code class="language-php">//Retrieve results as an object and output the results...
$users = $db-&gt;get_results( "SELECT username, email FROM users", true );
foreach( $users as $user )
{
    echo $user-&gt;username .' can be contacted at '. $user-&gt;email .'&lt;br /&gt;';
}

//Retrieve results as an ARRAY (default) and output the results...
$users = $db-&gt;get_results( "SELECT username, email FROM users" );
foreach( $users as $user )
{
    echo $user['username'] .' can be contacted at '. $user['email'] .'&lt;br /&gt;';
}
</code></pre>

        <h2>Retrieving data from the database</h2>
        <pre><code class="language-php">$query = "SELECT group_name FROM example_phpmvc";
$results = $db-&gt;get_results( $query );
foreach( $results as $row )
{
    echo $row['group_name'];
}</code></pre>

        <h2>Retrieving a single row of data</h2>
        <p>This example assumes usage where variables are being assigned using PHP's "list()" function</p>
        <pre><code class="language-php">$query = "SELECT group_id, group_name, group_parent FROM example_phpmvc WHERE group_name LIKE '%production%'";
list( $id, $name, $parent ) = $database-&gt;get_row( $query );
//Will return: 15, Production Tools, 8</code></pre>


        <h2>Determining the number of results from a query</h2>
        <p>Equivalent to mysql_num_rows</p>
        <pre><code class="language-php">$number = $database-&gt;num_rows( "SELECT group_name FROM example_phpmvc" );
echo $number;</code></pre>

        <h2>Inserting data into the database</h2>
        <p>Takes 2 parameters: table name, and an array of field =&gt; values</p>
        <pre><code class="language-php">//The fields and values to insert
$names = array(
    'group_parent' =&gt; 18,
    'group_name' =&gt; 'Awesomeness'
);
$add_query = $database-&gt;insert( 'example_phpmvc', $names );</code></pre>

        <h2>Inserting Multiple Records with a Single Query</h2>
        <pre><code class="language-php">//List of the database columns
$fields = array(
    'name', 
    'email', 
    'active'
);
//Array of records, where each row to be inserted is an inner array
$records = array(
   array(
        'Bennett', 'bennett@email.com', 1
    ), 
    array(
        'Lori', 'lori@email.com', 0
    ), 
    array(
        'Nick', 'nick@nick.com', 1, 'This will not be added' //Column count mismatch data is ignored
    ), 
    array(
        'Meghan', 'meghan@email.com', 1
    )
);
$db-&gt;insert_multi( 'users_table', $fields, $records );</code></pre>

        <p>To retrieve errors and messages from the query, use $variable-&gt;display() format.  First parameter refers to database operation, optional second parameter to display or store in variable:</p>
        
        <pre><code class="language-php">$db-&gt;display( $add_query [,true|false] );</code></pre>

        <h2>Retrieve the last insert id</h2>
        <p>
        </p><pre><code class="language-php">$last_id = $db-&gt;lastid();</code></pre>
        <p></p>

        <h2>Update database rows</h2>

        <p>Takes 4 parameters:
            </p><ol>
                <li>table name</li>
                <li>array of field =&gt; values to update</li>
                <li>array of field =&gt; equals values for the "where" clause.</li>
                <li>int maximum number of fields to update (optional)</li>
            </ol>    
        <p></p>
        <pre><code class="language-php">//Fields and values to update
$update = array(
    'name' =&gt; 'Awesome', 
    'city' =&gt; 'Brooklyn'
);

//Add the WHERE clauses
$where_clause = array(
    'name' =&gt; 'Bennett Stone', 
    'id' =&gt; 3
);
$updated = $db-&gt;update( 'example_phpmvc', $update, $where_clause, 1 );

//Output errors if they exist for the update query
$db-&gt;display( $updated );</code></pre>
        <p></p>

        <h2>Deleting rows from the database</h2>
        <p>Takes 3 parameters:
            </p><ol>
                <li>Table name</li>
                <li>The "where" clause as "column =&gt; value" array</li>
                <li>Number of rows to limit</li>
            </ol>
        <p></p>
        <p>
        </p><pre><code class="language-php">//Run a query to delete rows from table where id = 3 and name = Awesome, LIMIT 1
$delete = array(
    'id' =&gt; 3,
    'name' =&gt; 'Awesome'
);
$deleted = $db-&gt;delete( 'example_phpmvc', $delete, 1 );

//Display query
echo $db-&gt;display( $deleted );</code></pre>
        <p></p>

        <h2>Checking to see if a value exists</h2>
        <p>
        </p><pre><code class="language-php">$checksum = array( 'group_name' =&gt; 'bennett' );
$column_to_check = 'group_id';
$exists = $db-&gt;exists( $column_to_check, 'example_phpmvc',  $checksum );
if( $exists )
{
    echo 'Bennett DOES exist!';
}</code></pre>
        <p></p>

        <h2>Checking to see if tables exist</h2>

        <p>Sometimes helpful for determining install states.</p>
        <pre><code class="language-php">if( !$db-&gt;table_exists( 'example_phpmvc' ) )
{
    //Run a table install, the table doesn't exist
}</code></pre>

        <h2>Truncating database tables</h2>
        <p>Variables passed as array, multiple tables may be truncated.  Returns the number of tables truncated on echo.</p>
        <p>
        </p><pre><code class="language-php">//Truncate a single table, no output display
$truncate = $db-&gt;truncate( array('example_phpmvc') );

//Truncate multiple tables, display number of tables truncated
echo $db-&gt;truncate( array('example_phpmvc', 'my_other_table') );</code></pre>
        <p></p>

        <h2>List Fields in a Table</h2>
        <p>
        </p><pre><code class="language-php">$fields = $db-&gt;list_fields( "SELECT * FROM example_phpmvc" );
echo '&lt;pre&gt;';
print_r( $fields );
echo '&lt;/pre&gt;';</code></pre>
        <p></p>

        <h2>List the number of fields in a Table</h2>
        <p>
        </p><pre><code class="language-php">echo $db-&gt;num_fields( "SELECT * FROM example_phpmvc" );</code></pre>
        <p></p>

        <h2>Passing database query data to a view from a controller</h2>
        <p>Sometimes (frequently), it makes sense to load as much content as possible into the same view.  This is most helpful on websites that have many pages, all with the same structure.</p>
        <p>Database query data can easily be passed from the controller to the view by simply passing an array as the second parameter of the "view( 'template', $array_with_data )"</p>

            <p>The above query references a query made inside /application/controllers/controller-databases.php (same query parameters and technique applies if the query is loaded inside this file [<?php echo basename(__FILE__); ?>]):
        </p><pre><code class="language-php">function controller_databases()
{
    global $db;
    $query = "SELECT group_name FROM example_phpmvc ORDER BY RAND() LIMIT 5";
    $results = $db-&gt;get_results( $query );
    $body_content['results'] = $results;
    $body_content['counter'] = $db-&gt;num_rows( "SELECT group_id FROM example_phpmvc" );

    $header['title'] = 'Usage';
    $header['keywords'] = 'rapidphpme, rapid development, php, bennett stone';
    $header['description'] = 'RapidPHPMe is a PHP MVC framework for rapid development, no strings attached!';

    view( 'header', $header );

    view( 'databases', $body_content );

    view( 'footer' );
}</code></pre>

        <h2>Don't like the simplicy?</h2>
        <p>No problem! Running standard queries outside of the aforementioned may be done via the "query" function</p>
        <p>
        </p><pre><code class="language-php">//Normalized query
$query = "INSERT INTO example_phpmvc (group_name) VALUES ('Bennett')";
$db-&gt;query( $query );</code></pre>
        <p></p>
    </div>
</div>