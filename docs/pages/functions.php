<?php
/**
 * functions.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 18-Jul-2014
 * @package RapidPHPDocs
 **/
?>
<div class="row">
    <div class="medium-12 columns">
        <h1>Included Functions &amp; Reference</h1>
    </div>
</div>

<div class="row">
    <div class="medium-12 columns">
        <p><i>Of note, sessions are started by default in /index.php, so it is recommended to use:<br>
        </i></p>
        <pre><code class="language-php">if( !session_id() )
{
    session_start();
}</code></pre>

        As a check whenever handling sessions elsewhere.<p></p>

        <h2>Predefined constant values:</h2>
        <ol class="featurelist">
            <li>BASE (outputs the base URI of the site).  Defined in "/index.php".</li>
            <li>ROOT (outputs the absolute path of the site).  Defined in "/index.php".</li>
            <li>CLASSES (outputs the absolute path to the directory containing classes (default is "/includes/")).  Defined in "/index.php".</li>
            <li>HELPERS (outputs the absolute path to the helpers directory, subdir of classes).  Default is "/includes/helpers/"</li>
            <li>VIEWS (outputs the absolute path to the "/views/" directory containing view files).  Defined in "/index.php"</li>
            <li>MODE (production, development options).  Defined in "/index.php"</li>
            <li>DEBUG (TRUE/FALSE).  Displays all get and post values (output in "/views/layout/layout.php").  Defined in "/index.php"</li>
            <li>SITENAME (Default output for class.querystring.php page titles).  Defined in "/includes/config.php"</li>
            <li>PAGE_SEP (Default title seperator for class.querystring.php page titles).  Defined in "/config.php"</li>
            <li>SUBDIR (True/False).  TRUE requires additional parameter for SITE_DIRECTORY constant.  Both SUBDIR and SITE_DIRECTORY are defined in "/config.php"</li>
            <li>SITE_DIRECTORY (path to directory if SUBDIR is TRUE and installation is not in root of web installation).  Defined in "/config.php"</li>
        </ol>

        <h2>Predefined variables:</h2>
        <ol class="featurelist">
            <li>$autoload</li>
            <li>$connection</li>
            <li>$config</li>
        </ol>
    </div>
</div>