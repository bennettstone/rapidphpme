<?php
/**
 * files.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 18-Jul-2014
 * @package RapidPHPDocs
 **/
?>
<div class="row">
    <div class="medium-12 columns">
        <h1>File Handling</h1>
    </div>
</div>

<div class="row">
    <div class="medium-12 columns">
        <p>The functions contained in "/includes/helpers/files-helper.php" are designed to simplify file handling and management, and currently support:
        </p>
        <ul class="featurelist">
            <li>Creating files and adding content: write( filename, content )</li>
            <li>Appending content to existing files: append( filename, content )</li>
            <li>Deleting files: destroy( filename )</li>
            <li>Acquiring file contents: read( filename [,int $skip_empty = false][,string $return_as = 'array', 'string' ])</li>
            <li>Read the contents of a CSV file: getcsv( filename )</li>
        </ul>
        <p></p>

        <p>An absolute path is required to handle files (with the exception of the read function), i.e. "/var/www/images/filename.png", however, to simplify generation of the absolute path, simply call the ROOT, VIEWS, CLASSES, or HELPERS constants.</p>
        <pre><code class="language-php">$files = new includesHelper();
$files-&gt;add_helper( 'files' );

//Create and write to a file
$content = "This framework is amazing!";
$filename = ROOT .'/views/new-filename.txt';
write( $filename, $content );

//Append contents to the file created above (uses same $filename variable)
$new_content = "I am blown away!"
append( $filename, $new_content );

//Decide the file wasn't needed and delete it
destroy( $filename );

//Read the contents of a file into a string
//In this example, read the contents of the /views/home.php file
$read = read( VIEWS .'/home.php');

//Output using HTML entities to prevent the page from rendering
echo htmlentities( $read );

//Or, turn the file into an array and loop through
//Adding second parameter of "true" ignores empty and new lines
$readfile = read( VIEWS .'/home.php', true );
foreach( $readfile as $line_number =&gt; $line )
{
    echo $line_number .': '.htmlentities( $line ).'&lt;br /&gt;';
}

//Read the same file as above, returning contents as string
$readfile = read( VIEWS .'/home.php', true, true );
echo nl2br( htmlentities( $readfile ) );

//Get the contents of a CSV file, returns as array
$csv = getcsv( ROOT .'/test-csv-file.csv' );
&lt;pre&gt;
print_r( $csv );
&lt;/pre&gt;</code></pre>
    </div>
</div>