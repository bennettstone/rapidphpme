<?php
/**
 * helpers.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 18-Jul-2014
 * @package RapidPHPDocs
 **/
?>
<div class="row">
    <div class="medium-12 columns">
        <h1>Helpers</h1>
    </div>
</div>

<div class="row">
    <div class="medium-12 columns">
        
        <p>This package includes a helper set ("/includes/helpers/include-helper.php") designed to be a simple effective way to include additional classes and helpers.</p>

        <p>To include a new or existing class, simply put it into the "/includes[/helpers]" directory, then use either the add_helper(filename) or the add_class(filename) functions.  All files are assumed to be .php, and .php extensions are not necessary in the add_helper and add_class functions.</p>

        <p>To ensure consistency, helper files should follow strict naming conventions:

        </p><pre><code class="language-php">[callname][-helper.php]</code></pre>
            And <strong>only the prefix (prior to -helper.php) is used or needed to include the file</strong>
                <p></p>
                <p>Only 1 instance of the includeHelper() class should be initiated per file, even if multiple classes and helpers are included.  The following example demonstrates usage for including a new class, and a new helper in the same file.</p>
        <pre><code class="language-php">//Initiate the include class
$addfiles = new includeHelper();

//Include a new class with a filename of class.geocache.php
$addfiles-&gt;add_class( 'class.geocache' );

//Include a new helper named email-helper.php
$addfiles-&gt;add_helper( 'email' );

//Include the URL helper (url-helper.php)
$addfiles-&gt;add_helper( 'url' );

//Display contents of class directory
echo '&lt;pre&gt;';
print_r( $addfiles-&gt;list_classes() );
echo '&lt;/pre&gt;';

//Display contents of helpers directory
echo '&lt;pre&gt;';
print_r( $addfiles-&gt;list_helpers() );
echo '&lt;/pre&gt;';</code></pre>
    </div>
</div>