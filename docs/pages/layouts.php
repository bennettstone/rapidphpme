<?php
/**
 * layouts.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 18-Jul-2014
 * @package RapidPHPDocs
 **/
?>
<div class="row">
    <div class="medium-12 columns">
        <h1>Layouts &amp; Views</h1>
    </div>
</div>

<div class="row">
    <div class="medium-12 columns">
        <p>Ready for this?!...</p>

        <p>Let's say you want to add a page template for "Products", and have it accessible by going to yoursite.com/products/:</p>

        <ol class="featurelist">
            <li>Create a new file called "products.php"</li>
            <li>Put the "products.php" file into the "views" directory</li>
            <li>Go to yoursite.com/products/</li>
        </ol>

        <p>It is always recommended to have header, navigation, and footer files as separate entities within the /layouts/ directory, in which case, you may add those with a simple:</p>
        <pre><code class="language-php">&lt;?php
if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );
include 'layouts/header.php';
?&gt;

Your content here

&lt;?php include 'layouts/footer.php';</code></pre>
    </div>
</div>