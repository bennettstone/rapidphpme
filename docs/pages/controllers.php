<?php
/**
 * controllers.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 18-Jul-2014
 * @package RapidPHPDocs
 **/
?>
<div class="row">
    <div class="medium-12 columns">
        <h1>Controllers</h1>
    </div>
</div>

<div class="row">
    <div class="medium-6 columns">
        <h2>Using Controllers</h2>
        
        <p>In order to use controllers and pass data and view commands via a controller, a function must be created and included in an existing, or new file in the /application/controllers/ directory.  Controller files are autoloaded as long as they are in the /application/controllers/ directory and are valid php files.</p>

        <p>
            <strong>Controller functions must be prefixed with "controller_"</strong> in order to be loaded as valid controller functions, and the secondary part of the controller function name must match a file in the /application/views/ directory (i.e. controller_[PAGE]) <strong>UNLESS</strong> it has been mapped to a route (see <a href="index.php?page=routing">routing</a>).
        </p>

        <p>
            For example, to specify controller actions for the /application/views/about.php file, the following (although basic) example code would be placed into an existing, or new file in the /application/controllers/ directory.  The following code sample demonstrates how to load basic views, and pass a page title variable to the /application/views/layouts/header.php file:
        </p>
        <pre><code class="language-php">&lt;?php
/* 
 * application/controllers/controller-about.php
 * Will load /application/views/layouts/header.php, 
 * /navigation.php, 
 * /application/views/about.php, 
 * and /footer.php
 */
function controller_about()
{
    $params = array(
        'title' => SITENAME . PAGE_SEP . ' About this framework'
    );

    view( 'header', $params );
    view( 'navigation' );
    view( 'about' );
    view( 'footer' );
}</code></pre>
        
    </div>
    <div class="medium-6 columns">
        <h2>NOT Using Controllers</h2>
        <p>
            This is /application/views/controller-use.php.  This page has no controller in /application/controllers/, yet may (obviously) still be called via appropriate URI string.
        </p>
        <p>
            <strong>As long as the file requested from the URI exists in /application/views/, it will be loaded.</strong>
        </p>

        <p>
            The header, navigation, and footer have been manually included using normal "include", using the LAYOUTS constant to ensure appropriate inclusion paths.
        </p>
        <pre><code class="language-php">include( LAYOUTS . '/header.php' );
include( LAYOUTS . '/navigation.php' );</code></pre>

        <p>You can also skip all that and use the "view()" function which will find and load any matched file (<strong><em>view filenames must be unique!</em></strong>)</p>
        
        <pre><code class="language-php">view( 'header' );
view( 'navigation' );</code></pre>

        <p>
            The page titles and meta information may still be passed to the included files via their assigned variables...
        </p>
        <pre><code class="language-php">$title = SITENAME . PAGE_SEP . 'Controller Usage (or not)';
include( LAYOUTS . '/header.php' );
include( LAYOUTS . '/navigation.php' );</code></pre>

        <p>Or the same as above using the "view()" function by using an array- array contents are extracted so in the example below, 'title' can be accessed with $title:</p>
        <pre><code class="language-php">$params = array( 
    'title' => SITENAME . PAGE_SEP . 'Controller Usage (or not)'
);
view( 'header', $params );
view( 'navigation' );</code></pre>
    
    </div>
</div>