<?php
/**
 * quick-start.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 18-Jul-2014
 * @package RapidPHPDocs
 **/
?>
<div class="row">
    <div class="medium-12 columns">
        <h1>The RapidPHPMe Quick Start Guide</h1>
        <p>If you're ready to go, and you don't want to mess about with all the details, the here is what you need to know to get started and going right now!</p>
        <p>You can also just use the <a href="installer.php">mostly automatic installer</a> if you need to get going right now (<em>just make sure you delete it once it's been run!</em>).</p>
        <p>This framework comes bundled with some example layouts, controllers, and use scenarios for ease of use, and so I have less explaining to do, however, in order for the site to function properly, it is important to adjust the necessary configuration variables in index.php, and application/config/config.php.</p>
        <p>Example files are located in:</p>
        <ul>
            <li>/application/views/ and subdirectories</li>
            <li>/application/controllers/ and subdirectories</li>
            <li>/assets/ and subdirectories</li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="medium-12 columns">
        <h2>Step 1</h2>
        <p>Open the the 3 files in the /application/config/environments/, and adjust the following parameters for each conditional set:</p>
<pre><code class="language-php">define( 'DEBUG', true ); //Display output of some classes and functions, set to false for production
define( 'DISPLAY_DEBUG', true ); //Display error output, set to false for production
define( 'DB_ERRORLOG', true ); //Display database errors, set to false for production
define( 'LOG_ERRORS', true ); //Define if the errors should be written to a file, false for prod!
define( 'RUN_LOGS', true ); //Define if function trace logs should be generated to the /logs/ directory
define( 'SUBDIR', true ); //If the site is not in the PHP root, set this to true
define( 'PAGE_SEP', ' | ' ); //Default page title separator if using default header.php setup
define( 'SEND_ERRORS_TO', 'you@email.com' ); //Define the user who should receive error notification emails, remove to not send
define( 'FORCE_SSL', false ); //Enable to force https</code></pre>

        <p>These three files are loaded via ./index.php are are:</p>
        <ol>
            <li>/application/config/environments/development.php</li>
            <li>/application/config/environments/staging.php</li>
            <li>/application/config/environments/production.php</li>
        </ol>

        <h2>Step 2</h2>
        <p>There are 3 files that matter in /application/config/ (other than in /environments/):<br />
            <ol>
                <li>autoload.php: automatic inclusion of databases, user functions, helpers, etc...</li>
                <li>config.php: database connection details, secure hash keys, and any other custom config</li>
                <li>routes.php: pretty permalink customization, see the <a href="?page=routing">Routes &amp; URIs docs</a> for more info</li>
            </ol>
        <p><strong>If using databases, open /application/config/config.php and edit the following information for all 3 environments:</strong></p>
        <pre><code class="language-php">$connection['development']['db'] = 'localhost'; //the database server
$connection['development']['user'] = 'root'; //database username
$connection['development']['pass'] = 'root'; //database password
$connection['development']['dbname'] = 'process_db'; //database name</code></pre>

        <h2>Step 3</h2>
        <p>Add some files to the /application/views/ directory!</p>
        <p>No routes or controllers are needed unless you feel like using them, and URI paths will map directly to the name of any file in the /application/views/ directory.</p>
        <p><em>To use custom routes, controllers are required- only the first part of a request URI will automatically map to a view if no controller exists (aka, if you want the below example to be available at yoursite.com/public/contact/, a route and controller would be required).</em></p>
        <p>For example, to create a page available at www.yoursite.com/contact/, just create a file named &quot;contact.php&quot; anywhere in the /application/views/ directory, and it will be available at the proceeding URI.</p>
        
        <h3>Step 3.2</h3>
        <p>If controllers are necessary (which they often are for larger websites or highly dynamic content), you may create a controller with <strong>any filename that makes sense to you</strong> anywhere in the /application/controllers/ directory, and as long as it contains a function with the &quot;controller_[pagename]&quot; naming convention, it will automatically be loaded.</p>
        <p>Example of creating the same &quot;contact&quot; page using a controller...</p>
        <pre><code class="language-php">//File: /application/controllers/public/contact.php
//This would automatically be available at 'www.yoursite.com/contact/'
&lt;?php
function controller_contact()
{
    //Load the header.php file from /application/views/layouts/
    view( 'header' );
    
    //Load the contact.php file from /application/views/public
    view( 'contact' );
    
    //Load the footer.php file from /application/views/layouts/
    view( 'footer' );

}</code></pre>
        <p>If you wanted this to be available at a URI other than 'www.yoursite.com/contact/', simply add a route to the /application/config/routes.php file...</p>
        <pre><code class="language-php">//The 'function controller_' prefix is always ignored in routes
$routes['contact'] = array( 'public', 'contact' ); //Make the page available at 'www.yoursite.com/public/contact'

//Make the page available at an entirely different URI:
$routes['contact'] = array( 'public', 'random-pages', 'mapping' ); //Now at 'www.yoursite.com/public/random-pages/mapping'</code></pre>

        <h2>Quick Reference of Paths:</h2>
        <p>The following is a quick reference of paths at which items are available:</p>
        <pre><code class="language-php">//Assets such as CSS, JS, and Images have a full URI starting with the ASSETS constant...
&lt;link rel="stylesheet" href="&lt;?php echo ASSETS; ?&gt;css/styles.css"&gt;
//Load a javascript file
&lt;script src="&lt;?php echo ASSETS; ?&gt;js/scripts.js"&gt;&lt;/script&gt;

//Generate a link to the homepage
&lt;a href="&lt;?php echo BASE; ?&gt;">Go home&lt;/a&gt;

//Upload a file to the images directory
file_put_contents( ASSETS_ROOT .'images/new.jpg', $uploaded_file );

//Above file would be available at www.yoursite.com/assets/images/new.jpg
&lt;img src="&lt;?php echo ASSETS .'images/new.jpg'; ?&gt;" /&gt;
</code></pre>
    </div>
</div>