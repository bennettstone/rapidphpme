<?php
/**
 * routing.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 18-Jul-2014
 * @package RapidPHPDocs
 **/
?>
<div class="row">
    <div class="medium-12 columns">
        <h1>URI Routing</h1>
    </div>
</div>

<div class="row">
    <div class="medium-12 columns">
        <p>Since it's unlikely that you will ever want people to go to "mysite.com/whatever-directory/index.php?id=448" when in a world of wordpress (and this framework ;) ), the expectation is that your URI's are BEAUTIFUL and in (mostly) normal language.</p>

        <p>To make this whole process simpler, there are 2 possible routes:</p>
        <ol class="featurelist">
            <li>Extensive .htaccess rewrite rules</li>
            <li>Adding a couple lines to the /application/core/routes.php file</li>
        </ol>

        <p>And since I'm not super interested in maintaining crazy .htaccess rewrite rules, you can just add some funky fresh business to the routes.php file!</p>

        <p>The general format is:<br>

        </p><pre><code class="language-php">$routes['directory/view or controller function'] = array( 'URI part', 'URI part', ':get-variable', ':another-get-variable' );</code></pre>
        <p></p>

        <p>For example, http://yoursite.com/catalogue/archives/2013/06/25/ can be mapped to the /views/magazines/index.php by adding:</p>

        <pre><code class="language-php">$routes['magazines/index'] = array( 'catalogue', 'archives', ':year', ':month', ':day' );</code></pre>

        <p>Whereas, without custom mapping, the URI above would be accessible at: http://yoursite.com/magazines/index/?year=2013&amp;month=05&amp;day=25</p>
        
    </div>
</div>