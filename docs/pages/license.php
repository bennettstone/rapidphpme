<?php
/**
 * license.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 19-Jul-2014
 * @package RapidPHPDocs
 **/
?>
<div class="row">
    <div class="medium-12 columns">
        <h1>License</h1>
    </div>
</div>

<div class="row">
    <div class="medium-12 columns">
        <p>RapidPHPMe is free to use, modify, and extend under the GNU General Public License.</p>

        <p>COPYRIGHT (c) 2012 - <?php echo date( 'Y' ); ?> BENNETT STONE</p>

        <p>The source code included in this package is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation. This license can be read at:</p>

        <p>http://www.opensource.org/licenses/gpl-license.php</p>

        <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.</p>
    </div>
</div>