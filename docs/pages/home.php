<?php
/**
 * home.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 18-Jul-2014
 * @package RapidPHPDocs
 **/
?>
<div class="row">
    <div class="medium-12 columns">
        <h1>RapidPHP Docs</h1>
    </div>
</div>

<div class="row">
    <div class="medium-12 columns">
        <p>Love it or hate it, RapidPHPMe is designed from the ground up to be a simple, effective, dynamic, responsive, and easy to use PHP framework for super-rapid development.</p>

        <p>There is no learning curve. The documentation is there, and will take you approximately 5 minutes to read and understand. A dynamic, 5 page website can be created and configured in less than ten minutes.</p>
    </div>
</div>

<div class="row">
    <div class="medium-6 columns">
        <h2>The SUPEROVERVIEW</h2>
        <p>Take a quick peek at the <a href="?page=quick-start" alt="Quick start guide">quick start guide</a> for more advanced features and functionality such as the fast and slim <i>class.meta</i>, or the super amazing <i>class.db</i>, or just <strong>dive right into the /application/views/ directory</strong>, and start making a website already!</p>

        <p>You're looking at the "home.php" file right now (home.php), which is a beautiful beautiful file.</p>

        <p>By viewing the code for this file, and then taking a quick peek in the /application/controllers/ directory, you'll see that <strong>there is NO controller for this file</strong>, yet- here we are anyway!</p>

        <h4>This framework does not require controllers for views, but WILL use them if they exist.</h4>

        <p>Did I stutter?</p>
    </div>
    
    <div class="medium-6 columns">
        <h2>The features:</h2>
        
        <ul class="featurelist">
            <li>MVC Framework that does not require controllers</li>
            <li>Includes classes and helper functions such as:
                <ul>
                    <li>Class.db (magic and easy DB class, uses MySQLi)</li>
                    <li>Class.meta (to output everything from og:title's to CSS and JS)</li>
                    <li>Built in CRSF protection</li>
                    <li>Class.include (to easily include helpers and plugin files)</li>
                    <li>Extensive error class that handles PHP errors from undefined index to fatal, and will notify you via email when it happens</li>
                    <li>Auto-inclusion of view-files based on filename requests in the event that no controller exists</li>
                    <li>Simple filename based controller system with ZERO class extending required (in fact, it's a function and not a class, so good luck there!)</li>
                    <li>And much, much more!</li>
                </ul>
            </li>
        </ul>
        
    </div>
</div>

<div class="row">
    <div class="medium-4 columns">
        <h4>About</h4>
        <p>RapidPHPMe was created from the ground up to be a lightweight, responsive, MVC for <strong>actual</strong> rapid development.</p>
    </div>
    
    <div class="medium-4 columns">
        <h4>Support</h4>
        <p>Head over to the RapidPHPMe Github Repo and get your questions and answers thang on.</p>
    </div>
    
    <div class="medium-4 columns">
        <h4>Ready?</h4>
        <p>GOOD! Head over to the <a href="?page=quick-start">quick start guide</a> for the step by step to ensure success.</p>
    </div>
</div>