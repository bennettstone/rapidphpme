<?php
/**
 * encryption.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 24-Nov-2014
 * @package RapidPHPDocs
 **/
?>
<div class="row">
    <div class="medium-12 columns">
        <h1>Encryption</h1>
        <p>RapidPHP provides an interface for strong AES encryption via the mcrypt PHP extension using <a href="http://phpseclib.sourceforge.net/" target="_blank">phpseclib</a>.</p>
            
        <p>Be sure to set a secure string as the encryption key in /application/config/config.php in the <em>E_ST</em> constant that is 16, 24, or 32 characters long, otherwise encrypted values will not actually be secure!</p>
    </div>
</div>

<div class="row">
    <div class="medium-6 columns">
        <h2>Encrypting Values</h2>
        
        <pre><code class="language-php">$encrypted = Crypt::encrypt( 'string' );</code></pre>
        
    </div>
    <div class="medium-6 columns">
        <h2>Decrypting Values</h2>
        
        <pre><code class="language-php">$decrypted = Crypt::decrypt( $encrypted_value );</code></pre>
    
    </div>
</div>