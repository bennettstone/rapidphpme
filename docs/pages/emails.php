<?php
/**
 * emails.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 18-Jul-2014
 * @package RapidPHPDocs
 **/
?>
<div class="row">
    <div class="medium-12 columns">
        <h1>Emails</h1>
    </div>
</div>

<div class="row">
    <div class="medium-12 columns">
        <p>This package includes the <a href="https://github.com/PHPMailer/PHPMailer" target="_blank">PHPMailer Class</a> in order to simplify and extend mail sending functionality.</p>

        <p>Outgoing messages must contain "from", "to", "subject", and "message" variables.<br>
        The PHPMailer class must be included, then separately initiated in order to send messages.<br>
        The following example demonstrates a processing script from a simple form:</p>
        <pre><code class="language-php">//Check to see if the form was submitted prior to including the class
if( isset( $_POST['send'] ) )
{
    //Initiate the include helper and add the mailer class
    $mailer = new includeHelper();
    $mailer-&gt;add_class( 'class.phpmailer' );

    //Initiate the mailer class
    $mail = new PHPMailer();
    //Set the sender and receiver email addresses
    $mail-&gt;SetFrom( $_POST['sender'], "" );
    $mail-&gt;AddAddress( $_POST['receiver'], "" );

    //Set the message subject
    $mail-&gt;Subject = stripslashes( $_POST['subject'] );

    //Send the message as HTML
    $mail-&gt;MsgHTML( stripslashes( $_POST['message'] ) ); 

    //Display success or error messages
    if( !$mail-&gt;Send() )
        echo 'Message send failure: ' . $mail-&gt;ErrorInfo;
    else
        echo 'Message sent';
}</code></pre>
    </div>
</div>