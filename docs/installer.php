<?php
/**
 * installer.php
 * Provides easier installation and config for Tom!
 *
 *
 * @version 2.1
 * @date 18-Jul-2013
 * @updated 30-Jun-2014
 * @package RapidPHPMe
 **/

if( isset( $_POST ) && !empty( $_POST ) )
{
    require( 'installer/process-install.php' );
}

$environmental_params = array(
    'debug' => 'true', 
    'display_debug' => 'true', 
    'db_errorlog' => 'true', 
    'log_errors' => 'true', 
    'subdir' => 'false', 
    'dirname' => '', 
    'page_sep' => ' | ', 
    'send_errors_to' => 'you@email.com', 
    'site_url' => '', 
    'force_ssl' => 'false'
);

$selectors = array(
    '', 
    'true', 
    'false'
);
$db_options = array(
    'db', 
    'user', 
    'pass', 
    'dbname'
);
?>
<?php include_once( 'layouts/header.php' ); ?>
<?php include_once( 'layouts/nav.php' ); ?>
        
        <?php
        if( isset( $save_status ) && !empty( $save_status ) )
        {
            echo '<div class="row">';
            foreach( $save_status as $s )
            {
                echo '<div data-alert class="alert-box success radius">
                  '.$s.'
                  <a href="#" class="close">&times;</a>
                </div>';
            }
            echo '</div>';
        }
        ?>

        <div class="row">
            <div class="medium-12 columns">
                <h1>To install, complete the information below.</h1>
                <p><strong>DELETE this file once you have finished setup!</strong></p>
            </div>
        </div>
        
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="install">

        <div class="row">
            <div class="medium-12 columns">
                <h3>Global Settings</h3>
            </div>
        </div>
        <div class="row">
            <div class="medium-6 columns">
                <label for="sitename">Website Name</label>
                <input type="text" name="config[sitename]" class="required" value="<?php if( !empty( $_POST['config']['sitename'] ) ) echo $_POST['config']['sitename']; ?>" />
            </div>
            <div class="medium-6 columns">
                <label for="sitename">Database Prefix (optional)</label>
                <input type="text" name="config[prefix]" class="" value="<?php if( !empty( $_POST['config']['prefix'] ) ) echo $_POST['config']['prefix']; ?>" />
            </div>
            <div class="medium-12 columns">
                <label for="sitename">Encryption Key (grab one <a href="javascript://" onClick="window.open('https://api.wordpress.org/secret-key/1.1/salt/', '', 'width=600, height=400');return false;">here</a>)</label>
                <input type="text" name="config[e_st]" class="" value="<?php if( !empty( $_POST['config']['e_st'] ) ) echo $_POST['config']['e_st']; ?>" />
            </div>
        </div>
        
        <div class="row">
            <div class="medium-12 columns">     
                <h3>Environment Specific Settings</h3>
            </div>
        </div>
        <div class="row">
            <div class="medium-12 columns">
                <h4>Local</h4>
            </div>
        </div>
        <div class="row">
        <?php
        foreach( $environmental_params as $label => $value )
        {
            echo '<div class="medium-6 columns">';
            if( $value == 'true' || $value == 'false' )
            {
                //checkbox
                echo '<label>'.str_replace( '_', ' ', ucwords( $label ) ) .' ';
                echo '<select name="index[development]['.$label.']">';
                foreach( $selectors as $v )
                {
                    $checked = '';
                    if( (string)$value == (string)$v )
                    {
                        $checked = ' selected="selected"';
                    }
                    if( isset( $_POST['index']['development'][$label] ) && isset( $_POST['index']['development'][$label] ) == $v )
                    {
                        $checked = ' selected="selected"';
                    }
                    echo '<option value="'.$v.'"'.$checked.'>'.$v.'</option>';   
                }
                echo '</select>';
                echo '</label>';
            }
            else
            {
                echo '<label>';
                    $placeholder = '';
                    if( empty( $value ) )
                    {
                        $placeholder = ' placeholder="auto"';
                    }
                    echo str_replace( '_', ' ', ucwords( $label ) );
                    echo '<input type="text" name="index[development]['.$label.']" value="'.$value.'"'. $placeholder.' />'; 
                echo '</label>';
            }
            echo '</div>';
        }
        ?>
        </div>
    
        <div class="row">
            <div class="medium-12 columns">
                <hr />
                <p>If a database is necessary, enter the location and credentials here.</p>
            </div>
        </div>
        
        <div class="row">
        
        <?php
        foreach( $db_options as $k )
        {
            echo '<div class="medium-6 columns">';
            $value = '';
            if( isset( $_POST['config']['db']['development'][$k] ) )
            {
                $value = $_POST['config']['db']['development'][$k];
            }
            echo '<label>'. ucwords( $k ) .' ';
                echo '<input type="text" name="config[db][development]['.$k.']" value="'.$value.'" />';
            echo '</label>';
            echo '</div>';
        }
        ?>
        </div>
        
        <div class="row">
            <div class="medium-12 columns">
                <hr />
                <h4>Staging</h4>
            </div>
        </div>
        <div class="row">
        <?php
        foreach( $environmental_params as $label => $value )
        {
            echo '<div class="medium-6 columns">';
            if( $value == 'true' || $value == 'false' )
            {
                //checkbox
                echo '<label>'.str_replace( '_', ' ', ucwords( $label ) ) .' ';
                echo '<select name="index[staging]['.$label.']">';
                foreach( $selectors as $v )
                {
                    $checked = '';
                    if( (string)$value == (string)$v )
                    {
                        $checked = ' selected="selected"';
                    }
                    echo '<option value="'.$v.'"'.$checked.'>'.$v.'</option>';   
                }
                echo '</select>';
                echo '</label>';
            }
            else
            {
                echo '<label>';
                    $placeholder = '';
                    if( empty( $value ) )
                    {
                        $placeholder = ' placeholder="auto"';
                    }
                    echo str_replace( '_', ' ', ucwords( $label ) );
                    echo '<input type="text" name="index[staging]['.$label.']" value="'.$value.'"'. $placeholder.' />'; 
                echo '</label>';
            }
            echo '</div>';
        }
        ?>
        </div>
        
        <div class="row">
            <div class="medium-12 columns">
                <hr />
                <p>If a database is necessary, enter the location and credentials here.</p>
            </div>
        </div>
        <div class="row">
        <?php
        foreach( $db_options as $k )
        {
            echo '<div class="medium-6 columns">';
            $value = '';
            if( isset( $_POST['config']['db']['staging'][$k] ) )
            {
                $value = $_POST['config']['db']['staging'][$k];
            }
            
            echo '<label>'. ucwords( $k ) .' ';
                echo '<input type="text" name="config[db][staging]['.$k.']" value="'.$value.'" />';
            echo '</label>';
            echo '</div>';
        }
        ?>
        </div>
        <div class="row">
            <div class="medium-12 columns">
                <hr />
                <h4>Production</h4>
            </div>
        </div>
        <div class="row">
        <?php
        foreach( $environmental_params as $label => $value )
        {
            echo '<div class="medium-6 columns">';
            if( $value == 'true' || $value == 'false' )
            {
                //checkbox
                echo '<label>'.str_replace( '_', ' ', ucwords( $label ) ) .' ';
                echo '<select name="index[production]['.$label.']">';
                foreach( $selectors as $v )
                {
                    $checked = '';
                    if( (string)$value == (string)$v )
                    {
                        $checked = ' selected="selected"';
                    }
                    echo '<option value="'.$v.'"'.$checked.'>'.$v.'</option>';   
                }
                echo '</select>';
                echo '</label>';
            }
            else
            {
                echo '<label>';
                    $placeholder = '';
                    if( empty( $value ) )
                    {
                        $placeholder = ' placeholder="auto"';
                    }
                    echo str_replace( '_', ' ', ucwords( $label ) );
                    echo '<input type="text" name="index[production]['.$label.']" value="'.$value.'"'. $placeholder.' />'; 
                echo '</label>';
            }
            echo '</div>';
        }
        ?>
        </div>
        <div class="row">
            <div class="medium-12 columns">
                <hr />
                <p>If a database is necessary, enter the location and credentials here.</p>
            </div>
        </div>
        <div class="row">
        <?php
        foreach( $db_options as $k )
        {
            echo '<div class="medium-6 columns">';
            $value = '';
            if( isset( $_POST['config']['db']['production'][$k] ) )
            {
                $value = $_POST['config']['db']['production'][$k];
            }
            echo '<label>'. ucwords( $k ) .' ';
                echo '<input type="text" name="config[db][production]['.$k.']" value="'.$value.'" />';
            echo '</label>';
            echo '</div>';
        }
        ?>
        </div>
        <div class="row">
            <div class="medium-12 columns">
                <hr />
                <h3>Include the following plugins and helpers</h3>
            </div>
        </div>
        <div class="row">
            <div class="medium-6 columns">
                <label> 
                    <input type="checkbox" name="autoload[]" value="database" /> 
                    Databases
                </label>
            </div>
            <div class="medium-6 columns">
                <label> 
                    <input type="checkbox" name="autoload[]" value="cache" /> 
                    Caching
                </label>
            </div>
            <div class="medium-6 columns">
                <label> 
                    <input type="checkbox" name="autoload[]" value="email" /> 
                    PHPMailer
                </label>
            </div>
            <div class="medium-6 columns">
                <label> 
                    <input type="checkbox" name="autoload[]" value="minifier" /> 
                    MagicMin
                </label>
            </div>
            <div class="medium-6 columns">
                <label> 
                    <input type="checkbox" name="autoload[]" value="users" /> 
                    User System
                </label>
            </div>
            <div class="medium-6 columns">
                <label> 
                    <input type="checkbox" name="autoload[]" value="slug" /> 
                    URI Slug Generator (requires DB)
                </label>
            </div>
        </div>
        
        <div class="row">
            <div class="medium-12 columns" style="margin-top: 30px;">
                <button type="submit" class="button expand" name="install">Install!</button>
            </div>
        </div>

        </form>
        
<?php
$updated = 'Updated '. date( 'F j, Y', filemtime( __FILE__ ) );
include_once( 'layouts/footer.php' );

/* End of file installer.php */
/* Location: ./installer.php */