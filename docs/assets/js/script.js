jQuery(document).ready(function($){
    
    /* FADE OVER ELEMENTS ------------- */
    /* Remove this if you don't need OR customize */
    $('body').delay(400).fadeIn(1000);
    
    $('body').on('click', '.top', function(){
        $('html, body').animate( { scrollTop: 0 }, 'slow' );
        return false;
    });
    
    $('body').on('click', '.toggle', function(){
        var thisArea = $(this).data('id');
        $('#' + thisArea).toggle();
        $('html,body').animate({scrollTop: $("#"+thisArea).offset().top},'slow');
    });

    /*
    $('pre').click(function(){
        $('pre').removeClass('pre-selected, copy-selected');
        $(this).addClass('pre-selected');
    });
    
    $('body').not('pre').click(function(){
        //$('pre').removeClass('pre-selected, copy-selected');
    });
    
    $('pre').clipboard({
        // Return copying string data to clipboard
        copy : function() { 
            $('pre').removeClass('pre-selected');
            $('pre').removeClass('copy-selected');
            $(this).find('code').addClass('copy-selected');
            return $(this).closest('code').text();
        }
    });
    */

});