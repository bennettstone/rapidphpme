<?php
/**
 * index.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 18-Jul-2014
 * @package RapidPHPDocs
 **/
$page = 'home';
$title = 'Welcome';

if( isset( $_GET['page'] ) )
{
    $page = trim( strtolower( $_GET['page'] ) );
    if( file_exists( 'pages/'. $page .'.php' ) )
    {
        $page = $page;
        $title = ucfirst( str_replace( '-', ' ', $page ) );
    }
    else
    {
        $page = 'home';
    }
}

//Var for file to include, obviously defaults to home.php
$include_file = 'pages/'. $page .'.php';

//Add header
include_once( 'layouts/header.php' );

//Add nav
include_once( 'layouts/nav.php' );

//Add view
include_once( $include_file );

//Updated timestamp for page currently being viewed
$updated = 'Updated '. date( 'F j, Y', filemtime( $include_file ) );

//Add footer
include_once( 'layouts/footer.php' );