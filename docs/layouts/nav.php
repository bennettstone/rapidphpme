<?php
/**
 * nav.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 18-Jul-2014
 * @package RapidPHPDocs
 **/
?>
<div class="contain-to-grid fixed">
    <nav class="top-bar" data-topbar>
        <ul class="title-area">
            <li class="name">
            <h1><a href="index.php?page=home">RapidPHPDocs</a></h1>
            </li>
            <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
        </ul>

        <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul class="right">
                <li><a href="index.php?page=quick-start">Quick Start</a></li>
                <li><a href="installer.php">Auto-Installer</a></li>
                <li class="has-dropdown">
                <a href="#">Documentation</a>
                <ul class="dropdown">
                    <li class="has-dropdown"><a href="#">Files &amp; Pages</a>
                        <ul class="dropdown">
                            <li><a href="index.php?page=layouts">Layouts & Views</a></li>
                            <li><a href="index.php?page=controllers">Controllers</a></li>
                            <li><a href="index.php?page=routing">Routes & URIs</a></li>
                            <li><a href="index.php?page=autoload">Autoloading Files</a></li>
                            <li><a href="index.php?page=files">File Handling</a></li>
                        </ul>
                    </li>
                    <li class="has-dropdown"><a href="#">Data Handling</a>
                        <ul class="dropdown">
                            <li><a href="index.php?page=encryption">Data Encryption</a></li>
                            <li><a href="index.php?page=databases">Databases</a></li>
                            <li><a href="index.php?page=users">User System</a></li>
                        </ul>
                    </li>
                    <li class="has-dropdown"><a href="#">Helpers &amp; Classes</a>
                        <ul class="dropdown">
                            <li><a href="index.php?page=helpers">Helpers</a></li>
                            <li><a href="index.php?page=meta">metaHelper Class</a></li>
                            <li><a href="index.php?page=caching">Caching</a></li>
                            <li><a href="index.php?page=emails">Emails</a></li>
                            <li><a href="index.php?page=functions">Function Reference</a></li>
                            <li><a href="index.php?page=assets">JS & CSS Handling</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            </ul>
        </section>
    </nav>
</div>
<div style="margin-bottom: 20px;"></div>