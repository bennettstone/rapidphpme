<?php
/**
 * footer.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 18-Jul-2014
 * @package RapidPHPDocs
 **/
?>
    <footer class="row">
        <div class="medium-12 columns">
            <hr />
            <p>Docs updated <?php echo $updated; ?> | <a href="?page=license">License</a></p>
        </div>
    </footer>
    <script src="assets/js/vendor/jquery.js"></script>
    <script src="assets/js/foundation.min.js"></script>
    <script src="assets/js/jquery-clipboard.js"></script>
    <script src="assets/js/vendor/prism.js"></script>
    <script src="assets/js/script.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>