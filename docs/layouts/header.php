<?php
/**
 * header.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 18-Jul-2014
 * @package RapidPHPDocs
 **/
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo isset( $title ) ? $title : 'Welcome'; ?> | RapidPHPMe Documentation</title>
    <link rel="stylesheet" href="assets/css/foundation.css" />
    <link rel="stylesheet" href="assets/css/styles.css" />
    <link rel="stylesheet" href="assets/css/prism.css" />
    <script src="assets/js/vendor/modernizr.js"></script>
  </head>
  <body>