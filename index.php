<?php
/**
 * index.php
 * Loads primary site files
 * Evaluates query string variables
 * Defines global vars
 *
 * @version 2.1
 * @date 18-Jul-2013
 * @updated 01-Dec-2014
 * @package RapidPHPMe
 **/

/**
 * For automatic deployment via deployhq (www.deployhq.com)
 * Add the deployhq provided hook url to github as:
 * https://github.com/[yourusername]/[your-repository]/settings/hooks as a webhook URL
 */

//Ensure the ROOT constant is defined
define( 'ROOT', dirname( __FILE__ ) );
 
//Generic directory separator
define( 'SEP', DIRECTORY_SEPARATOR );

//Autoload the stuffs and handle anything we throw at this bad boy
$auto = ROOT . SEP . 'vendor/autoload.php';
if( !file_exists( $auto ) )
{
    echo '<p>This framework requires composer.</p>';
    echo '<p>To get started:</p>';
    echo '<ul>';
    echo '<li>Open terminal (or command prompt for windows users)</li>';
    echo '<li>type "cd" and drag this folder into the terminal window, then hit enter to make this the current working directory</li>';
    echo '<li>enter: "php composer.phar install"</li>';
    echo '<li>done</li>';
    echo '</ul>';
    die;
}
require( ROOT . SEP . 'vendor/autoload.php' );

$app = new App();

/* End of file index.php */
/* Location: ./index.php */